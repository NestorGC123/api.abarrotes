﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.Data.Entities
{
   public class SegmentacionPerfil
   {
      public string Zona { get; set; }
      public string Poblacion { get; set; }
      public int Valor { get; set; }
      public double Porcentaje { get; set; }
   }
}
