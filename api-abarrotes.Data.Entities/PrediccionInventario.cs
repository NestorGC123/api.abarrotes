﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.Data.Entities
{
   public class PrediccionInventario
   {
      public string ProductId { get; set; }
      public string ProductName { get; set; }
      public int Semana { get; set; }
      public string StoreId { get; set; }
      public int Prediction { get; set; }
      public int Year { get; set; }
   }
}
