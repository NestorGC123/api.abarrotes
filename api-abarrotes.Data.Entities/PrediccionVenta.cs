﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.Data.Entities
{
   public class PrediccionVenta
   {
      public string StoreId { get; set; }
      public int Semana { get; set; }
      public int Year { get; set; }
      public double PrediccionVentasSum { get; set; }
   }
}
