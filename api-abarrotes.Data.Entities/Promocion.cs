﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.Data.Entities
{
   public class Promocion
   {
      public string PromocionId { get; set; }
      public string StoreId { get; set; }
      public double CostoPromocion { get; set; }
      public string ProductId { get; set; }
      public string ProductName { get; set; }
      public string Prediction { get; set; }
      public string ProductNamePrediction { get; set; }
   }
}
