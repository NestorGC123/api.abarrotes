﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.Threading;

namespace api_abarrotes.Data.Db.OdbcProvider
{
   public class OdbcQueryExecutor : IDisposable
   {
      private bool Disposed = false;
      private OdbcConnection Connection { get; set; }
      public static OdbcQueryExecutor Create(string connectionStringName) => new OdbcQueryExecutor(connectionStringName);

      public OdbcQueryExecutor(string connectionStringName)
      {
         Connection = new OdbcConnection(ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString);
      }

      /// <summary>
      /// Executes a SQL query that returns a list of rows as the result.
      /// </summary>
      /// <param name="commandText">The HiveQL query to execute</param>
      /// <param name="parameters">Parameters to pass to the HiveQL query</param>
      /// <returns>A list of a Dictionary of Key, values pairs representing the 
      /// ColumnName and corresponding value</returns>
      public List<Dictionary<string, string>> Query(string commandText, Dictionary<string, object> parameters)
      {
         List<Dictionary<string, string>> rows = null;
         if (String.IsNullOrEmpty(commandText))
         {
            throw new ArgumentException("Command text cannot be null or empty.");
         }

         try
         {
            EnsureConnectionOpen();
            var command = CreateCommand(commandText, parameters);
            using (OdbcDataReader reader = command.ExecuteReader())
            {
               rows = new List<Dictionary<string, string>>();
               while (reader.Read())
               {
                  var row = new Dictionary<string, string>();
                  for (var i = 0; i < reader.FieldCount; i++)
                  {
                     var columnName = reader.GetName(i);
                     var columnValue = reader.IsDBNull(i) ? null : reader[i].ToString();//.GetString(i);
                     row.Add(columnName, columnValue);
                  }
                  rows.Add(row);
               }
            }
         }
         finally
         {
            EnsureConnectionClosed();
         }

         return rows;
      }

      /// <summary>
      /// Creates a HiveQLCommand with the given parameters
      /// </summary>
      /// <param name="commandText">The HiveQL query to execute</param>
      /// <param name="parameters">Parameters to pass to the HiveQL query</param>
      /// <returns></returns>
      private OdbcCommand CreateCommand(string commandText, Dictionary<string, object> parameters)
      {
         OdbcCommand command = Connection.CreateCommand();
         command.CommandText = commandText;
         AddParameters(command, parameters);

         return command;
      }

      /// <summary>
      /// Adds the parameters to a HiveQL command
      /// </summary>
      /// <param name="commandText">The HiveQL query to execute</param>
      /// <param name="parameters">Parameters to pass to the HiveQL query</param>
      private static void AddParameters(OdbcCommand command, Dictionary<string, object> parameters)
      {
         if (parameters == null)
         {
            return;
         }

         foreach (var param in parameters)
         {
            command.CommandText = command.CommandText.Replace(param.Key, param.Value.ToString());
         }
      }

      /// <summary>
      /// Opens a connection if closed
      /// </summary>
      private void EnsureConnectionOpen()
      {
         var retries = 3;
         if (Connection.State == ConnectionState.Open)
         {
            return;
         }
         else
         {
            while (retries >= 0 && Connection.State != ConnectionState.Open)
            {
               Connection.Open();
               retries--;
               Thread.Sleep(30);
            }
         }
      }

      /// <summary>
      /// Closes a connection if open
      /// </summary>
      public void EnsureConnectionClosed()
      {
         if (Connection.State == ConnectionState.Open)
         {
            Connection.Close();
         }
      }
      public virtual void Dispose(bool disposing)
      {
         if (!Disposed)
         {
            if (disposing)
            {
               Connection.Dispose();
               Connection = null;
            }
         }
         Disposed = true;
      }
      public void Dispose()
      {
         Dispose(true);
         GC.SuppressFinalize(this);
      }
   }
}
