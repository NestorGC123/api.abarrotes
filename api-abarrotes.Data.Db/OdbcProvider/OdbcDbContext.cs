﻿using System;
using System.Linq;

namespace api_abarrotes.Data.Db.OdbcProvider
{
   public class OdbcDbContext : IDisposable
   {
      private bool Disposed = false;
      public OdbcQueryExecutor Database { get; private set; }
      public OdbcDbContext(string connectionStringName)
      {
         Database = OdbcQueyExecutor.Create(connectionStringName);
         initializeOdbcDbSets();
      }
      protected virtual void initializeOdbcDbSets() { }
      public virtual OdbcDbSet<TEntity> GetOdbcDbSet<TEntity>() where TEntity : class, new()
      {
         var setProperty = this.GetType().GetProperties().Where(p => p.PropertyType.Equals(typeof(OdbcDbSet<TEntity>))).FirstOrDefault();
         
         if(setProperty != null)
         {
            return (OdbcDbSet<TEntity>) setProperty.GetValue(this);
         }

         return null;
      }
      public virtual void Dispose(bool disposing)
      {
         if (!Disposed)
         {
            Database.Dispose();
            Database = null;
         }
         Disposed = true;
      }
      public void Dispose()
      {
         Dispose(true);
         GC.SuppressFinalize(true);
      }
   }
}
