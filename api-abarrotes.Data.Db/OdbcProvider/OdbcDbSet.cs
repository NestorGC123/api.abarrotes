﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.Data.Db.OdbcProvider
{
   public class OdbcPropertyFieldMap
   {
      public string PropertyName { get; set; }
      public string FieldName { get; set; }
   }
   public class OdbcDbSet<T> where T : new()
   {
      private OdbcDbContext Context;
      public string TableName { get; private set; }
      public List<OdbcPropertyFieldMap> PropertyFieldMaps { get; private set; }

      public OdbcDbSet(OdbcDbContext database, string tableName, List<OdbcPropertyFieldMap> propertyFieldMaps)
      {
         Context = database;
         TableName = tableName;
         PropertyFieldMaps = propertyFieldMaps;
      }

      public List<T> GetAll()
      {
         Dictionary<string, object> parameters = new Dictionary<string, object>();
         parameters.Add("@ColumnNames", string.Join(",", PropertyFieldMaps.Select(p => p.FieldName).ToList()));
         parameters.Add("@TableName", TableName);

         List<Dictionary<string, string>> rows = Context.Database.Query("SELECT @ColumnNames FROM @TableName;", parameters);

         List<T> items = new List<T>();
         foreach (Dictionary<string, string> row in rows)
         {
            T item = new T();
            foreach (var cell in row)
            {
               string propertyName = PropertyFieldMaps.Where(pfm => pfm.FieldName == cell.Key.Replace(TableName + ".", "")).FirstOrDefault().PropertyName;
               var property = typeof(T).GetProperty(propertyName);
               property.SetValue(item, cell.Value);
            }
            items.Add(item);
         }

         return items;
      }
   }
}
