﻿using api_abarrotes.Data.Db.Context;
using api_abarrotes.Data.Entities;
using api_abarrotes.Utils;
using api_abarrotes.Utils.GenericClass;
using api_abarrotes.Utils.OdbcProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.Data.Db.UnitOfWork
{
   public class UoWAlgoriteamGeneral : IDisposable
   {
      private bool Disposed = false;
      private AlgoriteamDbContext Context { get; set; }
      public OdbcDbSet<PrediccionInventario> PredicionInventarioDbSet { get; set; }
      public OdbcDbSet<SegmentacionPerfil> SegmentacionPerfilDbSet { get; set; }
      public OdbcDbSet<PrediccionVenta> PrediccionVentaDbSet { get; set; }
      public OdbcDbSet<Promocion> PromocionDbSet { get; set; }

      public static UoWAlgoriteamGeneral Create(string connection) => new UoWAlgoriteamGeneral(connection);
      public UoWAlgoriteamGeneral(string connection)
      {
         Context = AlgoriteamDbContext.Create(connection);
         PredicionInventarioDbSet = Context.PrediccionesInventario;
         SegmentacionPerfilDbSet = Context.SegmentacionesPerfil;
         PrediccionVentaDbSet = Context.PrediccionesVenta;
         PromocionDbSet = Context.Promociones;
      }


      protected virtual void Dispose(bool Disposing)
      {
         if (!this.Disposed)
         {
            if (Disposing)
            {
               Context.Dispose();
            }
         }
         Disposed = true;
      }
      public void Dispose()
      {
         Dispose(true);
         GC.SuppressFinalize(this);
      }
   }
}
