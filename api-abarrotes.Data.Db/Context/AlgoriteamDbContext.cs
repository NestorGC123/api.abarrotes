﻿using api_abarrotes.Utils;
using api_abarrotes.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using api_abarrotes.Utils.OdbcProvider;

namespace api_abarrotes.Data.Db.Context
{
   public class AlgoriteamDbContext : OdbcDbContext
   {
      public static AlgoriteamDbContext Create(string connectionStringName) => new AlgoriteamDbContext(connectionStringName);
      public AlgoriteamDbContext(string connectionStringName) : base(connectionStringName) { }
      
      public OdbcDbSet<PrediccionInventario> PrediccionesInventario { get; private set; }
      public OdbcDbSet<PrediccionVenta> PrediccionesVenta { get; private set; }
      public OdbcDbSet<SegmentacionPerfil> SegmentacionesPerfil { get; private set; }
      public OdbcDbSet<Promocion> Promociones { get; private set; }

      protected override void initializeOdbcDbSets()
      {
         var prediccionInventarioPropertyFieldMaps = new List<OdbcPropertyFieldMap>()
         {
            new OdbcPropertyFieldMap()
            {
               PropertyName = nameof(PrediccionInventario.StoreId),
               FieldName = "store_id"
            },
            new OdbcPropertyFieldMap()
            {
               PropertyName = nameof(PrediccionInventario.ProductId),
               FieldName = "product_id"
            },
            new OdbcPropertyFieldMap()
            {
               PropertyName = nameof(PrediccionInventario.ProductName),
               FieldName = "product_name"
            },
            new OdbcPropertyFieldMap()
            {
               PropertyName = nameof(PrediccionInventario.Semana),
               FieldName = "semana"
            },
            new OdbcPropertyFieldMap()
            {
               PropertyName = nameof(PrediccionInventario.Prediction),
               FieldName = "prediction"
            },
            new OdbcPropertyFieldMap()
            {
               PropertyName = nameof(PrediccionInventario.Year),
               FieldName = "year"
            }
         };
         PrediccionesInventario = new OdbcDbSet<PrediccionInventario>(this, "predicciones_inventarios", prediccionInventarioPropertyFieldMaps);

         var prediccionVentaPropertyFieldMaps = new List<OdbcPropertyFieldMap>()
         {
            new OdbcPropertyFieldMap()
            {
               PropertyName = nameof(PrediccionVenta.StoreId),
               FieldName = "store_id"
            },
            new OdbcPropertyFieldMap()
            {
               PropertyName = nameof(PrediccionVenta.Semana),
               FieldName = "semana"
            },
            new OdbcPropertyFieldMap()
            {
               PropertyName = nameof(PrediccionVenta.PrediccionVentasSum),
               FieldName = "prediccion_ventas_sum"
            },
            new OdbcPropertyFieldMap()
            {
               PropertyName = nameof(PrediccionVenta.Year),
               FieldName = "year"
            }
         };
         PrediccionesVenta = new OdbcDbSet<PrediccionVenta>(this, "predicciones_ventas", prediccionVentaPropertyFieldMaps);
      
         var segmentacionPerfilPropertyFieldMaps = new List<OdbcPropertyFieldMap>()
         {
            new OdbcPropertyFieldMap()
            {
               PropertyName = nameof(SegmentacionPerfil.Zona),
               FieldName = "zona"
            },
            new OdbcPropertyFieldMap()
            {
               PropertyName = nameof(SegmentacionPerfil.Poblacion),
               FieldName = "poblacion"
            },
            new OdbcPropertyFieldMap()
            {
               PropertyName = nameof(SegmentacionPerfil.Valor),
               FieldName = "valor"
            },
            new OdbcPropertyFieldMap()
            {
               PropertyName = nameof(SegmentacionPerfil.Porcentaje),
               FieldName = "porcentaje"
            }
         };
         SegmentacionesPerfil = new OdbcDbSet<SegmentacionPerfil>(this, "segmentacion_perfiles", segmentacionPerfilPropertyFieldMaps);

         var promocionPropertyFieldMaps = new List<OdbcPropertyFieldMap>()
         {
            new OdbcPropertyFieldMap()
            {
               PropertyName = nameof(Promocion.StoreId),
               FieldName = "store_id"
            },
            new OdbcPropertyFieldMap()
            {
               PropertyName = nameof(Promocion.CostoPromocion),
               FieldName = "costo_promocion"
            },
            new OdbcPropertyFieldMap()
            {
               PropertyName = nameof(Promocion.ProductId),
               FieldName = "product_id"
            },
            new OdbcPropertyFieldMap()
            {
               PropertyName = nameof(Promocion.ProductName),
               FieldName = "product_name"
            },
            new OdbcPropertyFieldMap()
            {
               PropertyName = nameof(Promocion.Prediction),
               FieldName = "prediction"
            },
            new OdbcPropertyFieldMap()
            {
               PropertyName = nameof(Promocion.ProductNamePrediction),
               FieldName = "product_name_prediction"
            }
         };
         Promociones = new OdbcDbSet<Promocion>(this, "promociones", promocionPropertyFieldMaps);
      }
   }
}
