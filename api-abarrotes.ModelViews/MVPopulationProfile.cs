﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.ModelViews
{
   public class MVPopulationProfileItem
   {
      /// <summary>
      /// Population's age range for this profile
      /// </summary>
      public string AgeRange { get; set; }
      /// <summary>
      /// Number of people within this profile
      /// </summary>
      public int NumberOfPeople { get; set; }
      /// <summary>
      /// Percentage of people within this profile
      /// </summary>
      public string PercentageOfPeople { get; set; }
   }
   public class MVGetPopulationProfilesRequest
   {
      /// <summary>
      /// Store id for logging purposes
      /// </summary>
      [Required]
      public string StoreId { get; set; }
      /// <summary>
      /// Region to filter population profiles, It can be one of this strings: Norte, Sur, Centro, Oriente, Poniente
      /// </summary>
      [Required]
      public string Region { get; set; }
   }
   public class MVGetPopulationProfilesResponse
   {
      public int Total { get; set; }
      public List<MVPopulationProfileItem> Items { get; set; }
   }
}
