﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.ModelViews
{
  public class MVRoleItem
  {
    public string Id { get; set; }
    public string Name { get; set; }
  }

  public class MVGetAllRolesResponse
  {
    public List<MVRoleItem> Items { get; set; }
    public int Total { get; set; }
  }
  public class MVCreateRolesItem
  {
    [Required]
    public string RoleName { get; set; }
  }
  public class MVCreateRolesRequest
  {
    [Required]
    public List<MVCreateRolesItem> Items { get; set; }
  }
  public class MVCreateRolesResponse
  {
    public List<MVRoleItem> Items { get; set; }
  }
}
