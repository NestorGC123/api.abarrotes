﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.ModelViews
{
  public class MVBaseFilter
  {
    #region pagination
    /// <summary>
    /// Item number from where to start including items for the result page, defaults to 0
    /// </summary>
    public int Start { get; set; } = 0;

    /// <summary>
    /// Number of items to include in the result page, defaults to 10
    /// </summary>
    public int Size { get; set; } = 10;
    #endregion
  }
}
