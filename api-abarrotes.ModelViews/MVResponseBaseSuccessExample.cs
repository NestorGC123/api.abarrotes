﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.ModelViews
{
  public class MVResponseBaseSuccessExample<T>
  {
    public bool Status = true;
    public T Data = default(T);
  }
}
