﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.ModelViews
{
   public class MVPromotionItem
   {
      /// <summary>
      /// Promotion's id
      /// </summary>
      public string PromotionId { get; set; }
      /// <summary>
      /// Promotion's description, made of product names
      /// </summary>
      public string Description { get; set; }
      /// <summary>
      /// Summatory of product prices in this promotion, taking their quantity into account
      /// </summary>
      public string PromotionCost { get; set; }
      /// <summary>
      /// Products in this promotion
      /// </summary>
      public List<MVPromotionProductItem> Products { get; set; }
   }

   public class MVPromotionProductItem
   {
      /// <summary>
      /// Product's id
      /// </summary>
      public string ProductId { get; set; }
      /// <summary>
      /// Product's name
      /// </summary>
      public string ProductName { get; set; }
      /// <summary>
      /// Product's quantity in this promotion
      /// </summary>
      public int ProductQuantity { get; set; } = 1;
   }

   public class MVGetPromotionsRequest
   {
      /// <summary>
      /// Store id for which to get promotions
      /// </summary>
      [Required]
      public string StoreId { get; set; }
   }
   public class MVGetPromotionsResponse
   {
      public int Total { get; set; }
      public List<MVPromotionItem> Items { get; set; }
   }
}
