﻿using Swagger.Net;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.ModelViews
{
  public class MVBestsellerItem
  {
    public string Name { get; set; }
  }
  public class MVGetBestsellersRequest : MVBaseFilter
  {
    /// <summary>
    /// Store id for log purposes
    /// </summary>
    [Required]
    public string StoreId { get; set; }
    /// <summary>
    /// Region to which the bestsellers belong, It can be one of this strings: Norte, Sur, Centro, Oriente, Poniente
    /// </summary>
    [Required]
    public string Region { get; set; }
    /// <summary>
    /// Date to which the bestsellers belong, in ISO format and UTC time
    /// </summary>
    [Required]
    public string Date { get; set; }
  }

  public class MVGetBestsellersResponse
  {
    public int Total { get; set; }
    public List<MVBestsellerItem> Items { get; set; }
  }
}
