﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.ModelViews
{
   public class MVInventoryForecastItem
   {
      public string ProductId { get; set; }
      public string ProductName { get; set; }
      public int InventoryForecast { get; set; }
      public DateTime ValidFromDate { get; set; }
      public DateTime ValidUntilDate { get; set; }

   }
   public class MVGetInventoryForecastsRequest : MVBaseFilter
   {
      /// <summary>
      /// Store id for which to get inventory forecasts
      /// </summary>
      [Required]
      public string StoreId { get; set; }
      /// <summary>
      /// Date for which to get inventory forecasts, in ISO format and UTC time
      /// </summary>
      [Required]
      public DateTime ReferenceDate { get; set; } = DateTime.UtcNow;
   }

   public class MVGetInventoryForecastsResponse
   {
      public int Total { get; set; }
      public List<MVInventoryForecastItem> Items { get; set; }
   }
}
