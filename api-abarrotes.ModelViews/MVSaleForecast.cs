﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.ModelViews
{
   public class MVSaleForecastItem
   {
      public int SaleForecast { get; set; }
      public DateTime ValidFromDate { get; set; }
      public DateTime ValidUntilDate { get; set; }
      //public int ValidForWeekOfTheYear { get; set; }
      //public int ValidForYear { get; set; }
   }
   public class MVGetSaleForecastsRequest
   {
      /// <summary>
      /// Store id for which to get product inventory forecasts
      /// </summary>
      [Required]
      public string StoreId { get; set; }
      /// <summary>
      /// Specify how the sale forecasts should be aggregated, at this time the only valid string is: Semana
      /// </summary>
      [Required]
      public string AggregationType { get; set; }
      /// <summary>
      /// Date to start sale forecasts search, in ISO format and UTC time
      /// </summary>
      [Required]
      public DateTime FromDate { get; set; }
      /// <summary>
      /// Date to end sale forecasts search, in ISO format and UTC time
      /// </summary>
      [Required]
      public DateTime UntilDate { get; set; }
   }

   public class MVGetSaleForecastsResponse
   {
      public int Total { get; set; }
      public List<MVSaleForecastItem> Items { get; set; }
   }
}
