﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.ModelViews
{
  public class MVUserItem
  {
    public string Id { get; set; }

    public string UserName { get; set; }
  }

  public class MVGetAllUsersResponse
  {
    public List<MVUserItem> Items { get; set; }
    public int Total { get; set; }
  }

  public class MVGetCurrentUserResponse
  {
    public MVUserItem Item { get; set; }
  }

}
