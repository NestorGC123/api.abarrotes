﻿using api_abarrotes.User.Db.Context;
using api_abarrotes.User.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.DataProtection;


namespace api_abarrotes.User.Db.IdentityManager
{
  using AppUser = api_abarrotes.User.Entities.User;

  #region customs stores

  public class CustomUserStore : UserStore<AppUser, Role, string,
      UserLogin, UserRole, UserClaim>
  {
    public CustomUserStore(UserDbContext context)
        : base(context)
    {
    }
  }

  public class CustomRoleStore : RoleStore<Role, string, UserRole>
  {
    public CustomRoleStore(UserDbContext context)
        : base(context)
    {
    }
  }

  #endregion

  public class ApplicationUserManager : UserManager<AppUser, string>
  {
    public static ApplicationUserManager Create(IUserStore<AppUser, string> store)
    //public static ApplicationUserManager Create(IUserStore<AppUser, string> store, MailConfiguration config)
    {
      var manager = new ApplicationUserManager(store);
      manager.UserValidator = new UserValidator<AppUser, string>(manager)
      {
        AllowOnlyAlphanumericUserNames = false
        //RequireUniqueEmail = true
      };
      // Configure la lógica de validación de contraseñas
      //manager.PasswordValidator = new PasswordValidator
      //{
      //  RequiredLength = 6,
      //  RequireNonLetterOrDigit = true,
      //  RequireDigit = true,
      //  RequireLowercase = true,
      //  RequireUppercase = true,
      //};


      var provider = new DpapiDataProtectionProvider("api-abarrotes_AppName");
      manager.UserTokenProvider = new DataProtectorTokenProvider<AppUser, string>(provider.Create("api-abarrotes_TokenAppName"));

      return manager;
    }

    public ApplicationUserManager(IUserStore<AppUser, string> store)
            : base(store)
    {
    }

    public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
    {
      var manager = new ApplicationUserManager(new CustomUserStore(context.Get<UserDbContext>()));
      // Configure la lógica de validación de nombres de usuario
      manager.UserValidator = new UserValidator<AppUser, string>(manager)
      {
        AllowOnlyAlphanumericUserNames = false,
        //RequireUniqueEmail = true
      };
      // Configure la lógica de validación de contraseñas
      //manager.PasswordValidator = new PasswordValidator
      //{
      //  RequiredLength = 6,
      //  RequireNonLetterOrDigit = true,
      //  RequireDigit = true,
      //  RequireLowercase = true,
      //  RequireUppercase = true,
      //};


      var dataProtectionProvider = options.DataProtectionProvider;
      if (dataProtectionProvider != null)
      {
        manager.UserTokenProvider = new DataProtectorTokenProvider<AppUser, string>(dataProtectionProvider.Create("ASP.NET Identity"));
      }
      return manager;
    }


  }
}
