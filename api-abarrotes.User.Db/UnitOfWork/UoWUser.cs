﻿using api_abarrotes.User.Db.Context;
using api_abarrotes.User.Entities;
using api_abarrotes.User.Db.IdentityManager;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.User.Db.UnitOfWork
{
  public class UoWUser
  {
    public static UoWUser Create(string connection) => new UoWUser(connection);
    private UserDbContext Context { get; set; }

    public UoWUser(string connection)
    {
      Context = UserDbContext.Create(connection);
      UserManager = ApplicationUserManager.Create(
          store: new CustomUserStore(Context)); //new ApplicationUserManager(new CustomUserStore(Context));
      RoleManager = new RoleManager<Role>(new CustomRoleStore(Context));
    }

    public ApplicationUserManager UserManager { get; set; }
    public RoleManager<Role> RoleManager { get; set; }

    public void Save()
    {
      Context.SaveChanges();
    }

    private bool Disposed = false;

    protected virtual void Dispose(bool Disposing)
    {
      if (!Disposed)
      {
        if (Disposing)
        {
          Context.Dispose();
        }
      }
      Disposed = true;
    }

    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }
  }
}
