﻿using api_abarrotes.User.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace api_abarrotes.User.Db.Context
{
  using AppUser = Entities.User;

  public class UserDbContext : IdentityDbContext<AppUser, Role, string, UserLogin, UserRole, UserClaim>
  {
    public static UserDbContext Create(string connection) => new UserDbContext(connection);
    public static UserDbContext Create() => new UserDbContext();

    public UserDbContext(string connection) : base(connection) {    }
    public UserDbContext() : base("name=UserDbContext") {    }

    private string GetTableName(Type type)
    {
      var result = Regex.Replace(type.Name, ".[A-Z]", m => m.Value[0] + "_" + m.Value[1]);
      return result.ToLower();
    }

    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {
      //Por default en postgresql el schema es public, NO dbo como sql server.
      modelBuilder.HasDefaultSchema("public");
      //nombres de propiedades en minúscula como pide postgresql
      modelBuilder.Properties().Configure(p => p.HasColumnName(p.ClrPropertyInfo.Name.ToLower()));
      //nombre de tablas en minúscula como lo pide postgresql
      modelBuilder.Types().Configure(c => c.ToTable(GetTableName(c.ClrType)));
      //Precisión por defecto de decimales a menos que se especifique otro
      modelBuilder.Properties<decimal>().Configure(config => config.HasPrecision(10, 2));
      //Todos los id son claves primarias
      modelBuilder.Properties().Where(x => x.Name.ToLower().Equals("id")).Configure(config => config.IsKey());
      //modelBuilder.Properties<Byte[]>().Configure(conf => conf.HasColumnType("NpgsqlTypes.PostgisPoint"));
      //modelBuilder.ComplexType<object>().Property(o => o as NpgsqlTypes.PostgisGeometry o);
      modelBuilder.Types<object>();

      base.OnModelCreating(modelBuilder);

      var user = modelBuilder.Entity<AppUser>();
      user.ToTable("users");
      user.Property(x => x.Email).IsOptional();
      user.Property(x => x.Id).IsRequired();
      user.Property(x => x.Status);
      user.Property(x => x.CreateDate);
      user.Property(x => x.CreateBy).HasMaxLength(120).IsOptional();
      user.Property(x => x.UpdateDate);
      user.Property(x => x.UpdateBy).HasMaxLength(120).IsOptional();


      var role = modelBuilder.Entity<Role>();
      role.ToTable("roles");
      role.Property(x => x.Id).IsRequired();
      role.Property(x => x.Status);
      role.Property(x => x.CreateDate);
      role.Property(x => x.CreateBy).HasMaxLength(120).IsOptional();
      role.Property(x => x.UpdateDate);
      role.Property(x => x.UpdateBy).HasMaxLength(120).IsOptional();

      var userClaim = modelBuilder.Entity<UserClaim>();
      userClaim.ToTable("user_claims");
      userClaim.Property(x => x.Id).IsRequired();
      userClaim.Property(x => x.Status);
      userClaim.Property(x => x.CreateDate);
      userClaim.Property(x => x.CreateBy).HasMaxLength(120).IsOptional();
      userClaim.Property(x => x.UpdateDate);
      userClaim.Property(x => x.UpdateBy).HasMaxLength(120).IsOptional();

      var userRole = modelBuilder.Entity<UserRole>();
      userRole.ToTable("user_roles");

      var userLogin = modelBuilder.Entity<UserLogin>();
      userLogin.ToTable("user_logins");
    }

    //Los DbSet para User y Role son generados automaticamente
    public DbSet<UserRole> UserRoles { get; set; }
  }
}
