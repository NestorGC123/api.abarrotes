﻿namespace api_abarrotes.User.Db.Migrations
{
  using api_abarrotes.User.Entities;
  using api_abarrotes.User.Db.UnitOfWork;
  using api_abarrotes.Utils.Cofigurations;
  using api_abarrotes.Utils.Enums;
  using Microsoft.AspNet.Identity;
  using System;
  using System.Collections.Generic;
  using System.Data.Entity;
  using System.Data.Entity.Core.Objects;
  using System.Data.Entity.Migrations;
  using System.Linq;
  using UserEntity = Entities.User;

  internal sealed class Configuration : DbMigrationsConfiguration<api_abarrotes.User.Db.Context.UserDbContext>
  {
    UoWUser uoWUser { get; set; }

    public Configuration()
    {
      AutomaticMigrationsEnabled = false;
    }

    protected override void Seed(api_abarrotes.User.Db.Context.UserDbContext context)
    {
      uoWUser = UoWUser.Create(
          connection: GlobalConfigurations.USER_DB_CONNECTION);

      //Creates admin role
      Role adminRole = context.Roles.Where(r => r.Name == "admin").FirstOrDefault();
      if(adminRole is null)
      {
        adminRole = new Role() { Id = Guid.NewGuid().ToString(), Name = "admin", Status = (int)EntityStatusKey.CODES.NEW, CreateBy = "Seed", CreateDate = DateTime.Now };
        uoWUser.RoleManager.Create(adminRole);
      }

      //Creates user role
      Role userRole = context.Roles.Where(r => r.Name == "user").FirstOrDefault();
      if (userRole is null)
      {
        userRole = new Role() { Id = Guid.NewGuid().ToString(), Name = "user", Status = (int)EntityStatusKey.CODES.NEW, CreateBy = "Seed", CreateDate = DateTime.Now };
        uoWUser.RoleManager.Create(userRole);
      }

      //Creates admin user
      UserEntity adminUser = context.Users.Where(u => u.UserName == "admin").FirstOrDefault();
      if (adminUser is null)
      { 
        adminUser = new UserEntity()
        {
          Id = Guid.NewGuid().ToString(),
          UserName = "admin",
          CreateBy = "Seed",
          CreateDate = DateTime.Now,
          Status = (int)EntityStatusKey.CODES.NEW,
        };
        uoWUser.UserManager.Create(adminUser, "PlenumsoftEnergy2019");
        uoWUser.UserManager.AddToRole(adminUser.Id, adminRole.Name);
      }

      //Creates abarrotics user 
      UserEntity abarroticsUser = context.Users.Where(u => u.UserName == "abarrotics").FirstOrDefault();
      if (abarroticsUser is null)
      {
        abarroticsUser = new UserEntity()
        {
          Id = Guid.NewGuid().ToString(),
          UserName = "abarrotics",
          CreateBy = "Seed",
          CreateDate = DateTime.Now,
          Status = (int)EntityStatusKey.CODES.NEW,
        };
        uoWUser.UserManager.Create(abarroticsUser, "abarrotics2020");
        uoWUser.UserManager.AddToRole(abarroticsUser.Id, userRole.Name);
      }

      context.SaveChanges();
    }
  }
}
