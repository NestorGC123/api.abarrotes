﻿namespace api_abarrotes.User.Db.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "public.roles",
                c => new
                    {
                        id = c.String(nullable: false, maxLength: 128),
                        status = c.Int(),
                        createdate = c.DateTime(),
                        updatedate = c.DateTime(),
                        createby = c.String(maxLength: 120),
                        updateby = c.String(maxLength: 120),
                        name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.id)
                .Index(t => t.name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "public.user_roles",
                c => new
                    {
                        userid = c.String(nullable: false, maxLength: 128),
                        roleid = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.userid, t.roleid })
                .ForeignKey("public.roles", t => t.roleid, cascadeDelete: true)
                .ForeignKey("public.users", t => t.userid, cascadeDelete: true)
                .Index(t => t.userid)
                .Index(t => t.roleid);
            
            CreateTable(
                "public.users",
                c => new
                    {
                        id = c.String(nullable: false, maxLength: 128),
                        status = c.Int(),
                        createdate = c.DateTime(),
                        updatedate = c.DateTime(),
                        createby = c.String(maxLength: 120),
                        updateby = c.String(maxLength: 120),
                        email = c.String(maxLength: 256),
                        emailconfirmed = c.Boolean(nullable: false),
                        passwordhash = c.String(),
                        securitystamp = c.String(),
                        phonenumber = c.String(),
                        phonenumberconfirmed = c.Boolean(nullable: false),
                        twofactorenabled = c.Boolean(nullable: false),
                        lockoutenddateutc = c.DateTime(),
                        lockoutenabled = c.Boolean(nullable: false),
                        accessfailedcount = c.Int(nullable: false),
                        username = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.id)
                .Index(t => t.username, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "public.user_claims",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        status = c.Int(),
                        createdate = c.DateTime(),
                        updatedate = c.DateTime(),
                        createby = c.String(maxLength: 120),
                        updateby = c.String(maxLength: 120),
                        userid = c.String(nullable: false, maxLength: 128),
                        claimtype = c.String(),
                        claimvalue = c.String(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("public.users", t => t.userid, cascadeDelete: true)
                .Index(t => t.userid);
            
            CreateTable(
                "public.user_logins",
                c => new
                    {
                        loginprovider = c.String(nullable: false, maxLength: 128),
                        providerkey = c.String(nullable: false, maxLength: 128),
                        userid = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.loginprovider, t.providerkey, t.userid })
                .ForeignKey("public.users", t => t.userid, cascadeDelete: true)
                .Index(t => t.userid);
            
        }
        
        public override void Down()
        {
            DropForeignKey("public.user_roles", "userid", "public.users");
            DropForeignKey("public.user_logins", "userid", "public.users");
            DropForeignKey("public.user_claims", "userid", "public.users");
            DropForeignKey("public.user_roles", "roleid", "public.roles");
            DropIndex("public.user_logins", new[] { "userid" });
            DropIndex("public.user_claims", new[] { "userid" });
            DropIndex("public.users", "UserNameIndex");
            DropIndex("public.user_roles", new[] { "roleid" });
            DropIndex("public.user_roles", new[] { "userid" });
            DropIndex("public.roles", "RoleNameIndex");
            DropTable("public.user_logins");
            DropTable("public.user_claims");
            DropTable("public.users");
            DropTable("public.user_roles");
            DropTable("public.roles");
        }
    }
}
