﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using api_abarrotes.InteractorMessages;
using api_abarrotes.ModelViews;
using api_abarrotes.UserStories.RoleHandler;
using api_abarrotes.Utils.GenericClass;
using api_abarrotes.Utils.Helpers;
using Microsoft.AspNet.Identity;

namespace api_abarrotes.Controllers
{
  public class RoleController : ApiBaseController
  {
    public IRoleInteractor GetInteractor()
    {
      return RoleInteractor.Create(INTER_CONFIG);
    }

    [Authorize(Roles = "admin")]
    [HttpGet]
    [Route("~/api/roles")]
    public HttpResponseMessage GetAllRoles()
    {
      using (var interactor = GetInteractor())
      {
        var response = interactor.GetRoles(new GetRolesRequest()
        {
          //No hay ningun tipo de filtro
        });

        var httpResponse = Request.CreateResponse((HttpStatusCode)response.StatusCode, response.Status ? MVResponseBase<MVGetAllRolesResponse>.Create(new MVGetAllRolesResponse()
        {
          Items = MapperHelper.Map<List<MVRoleItem>>(response.Data.Items),
          Total = response.Data.Total
        }) : MVResponseBase<MVGetAllRolesResponse>.Create(response.Errors));

        //httpResponse.ReasonPhrase = String.Join(",", response.Errors);
        return httpResponse;
      }
    }

    [Authorize(Roles = "admin")]
    [HttpPost]
    [Route("~/api/roles")]
    public HttpResponseMessage CreateRoles([FromBody] MVCreateRolesRequest request)
    {
      if (!ModelState.IsValid)
      {
        List<string> errors = new List<string>();
        foreach (var parameter in ModelState.Values)
        {
          foreach (var error in parameter.Errors)
          {
            errors.Add(error.ErrorMessage.Trim('.'));
          }
        }

        var httpResponse = Request.CreateResponse(HttpStatusCode.BadRequest, MVResponseBase<MVCreateRolesResponse>.Create(errors));


        //httpResponse.ReasonPhrase = String.Join(",", errors);

        return httpResponse;
      }

      using (var interactor = GetInteractor())
      {

        var response = interactor.CreateRoles(new CreateRolesRequest
        {
          UserId = User.Identity.GetUserId(),
          Items = MapperHelper.Map<List<CreateRolesItem>>(request.Items)
        });

        var httpResponse = Request.CreateResponse((HttpStatusCode) response.StatusCode, response.Status ? MVResponseBase<MVCreateRolesResponse>.Create(new MVCreateRolesResponse()
        {
          Items = MapperHelper.Map<List<MVRoleItem>>(response.Data.Items)
        }) : MVResponseBase<MVCreateRolesResponse>.Create(response.Errors));

        //httpResponse.ReasonPhrase = String.Join(",", response.Errors);

        return httpResponse;

      }
    }
  }
}
