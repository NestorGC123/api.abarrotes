﻿using api_abarrotes.Utils.GenericClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Description;
using System.Web.Http.Dispatcher;

namespace api_abarrotes.Controllers
{
  /// <summary>
  /// + Class to override behavior when a url has not api structure or it's not found
  /// </summary>
  [ApiExplorerSettings(IgnoreApi = true)]
  public class NotFoundController : ApiController
  {
    [HttpGet, HttpPost, HttpPut, HttpDelete, HttpHead, HttpOptions, AcceptVerbs("PATCH")]
    public HttpResponseMessage Handle404()
    {

      var model = MVResponseBase<Object>.Create(new List<string> { "This endpoind doesn't exist, go to /swagger to see all available endpoints" });

      var responseMessage = Request.CreateResponse(HttpStatusCode.NotFound, model);
      //responseMessage.ReasonPhrase = "The requested endpoint was not found";

      return responseMessage;

    }
  }

  /// <summary>
  /// + Class to override api behavior when a controller is not found
  /// </summary>
  [ApiExplorerSettings(IgnoreApi = true)]
  public class HttpNotFoundAwareDefaultHttpControllerSelector : DefaultHttpControllerSelector

  {  

    public HttpNotFoundAwareDefaultHttpControllerSelector(HttpConfiguration configuration)

        : base(configuration)

    {

    }

    public override HttpControllerDescriptor SelectController(HttpRequestMessage request)

    {

      HttpControllerDescriptor decriptor = null;

      try

      {

        decriptor = base.SelectController(request);

      }

      catch (HttpResponseException ex)

      {

        var code = ex.Response.StatusCode;

        if (code != HttpStatusCode.NotFound)

          throw;

        var routeValues = request.GetRouteData().Values;

        routeValues["controller"] = "NotFound";

        routeValues["action"] = "Handle404";

        decriptor = base.SelectController(request);

      }

      return decriptor;
    }

  }

  /// <summary>
  /// + Class to override api behavior when an action is not found on a controller
  /// </summary>
  [ApiExplorerSettings(IgnoreApi = true)]
  public class HttpNotFoundAwareControllerActionSelector : ApiControllerActionSelector
  {
    public HttpNotFoundAwareControllerActionSelector()

    {

    }

    public override HttpActionDescriptor SelectAction(HttpControllerContext controllerContext)

    {

      HttpActionDescriptor decriptor = null;

      try

      {

        decriptor = base.SelectAction(controllerContext);

      }

      catch (HttpResponseException ex)

      {

        var code = ex.Response.StatusCode;

        if (code != HttpStatusCode.NotFound && code != HttpStatusCode.MethodNotAllowed)

          throw;
        var routeData = controllerContext.RouteData;

        routeData.Values["action"] = "Handle404";

        IHttpController httpController = new NotFoundController();

        controllerContext.Controller = httpController;

        controllerContext.ControllerDescriptor = new HttpControllerDescriptor(controllerContext.Configuration, "Error", httpController.GetType());

        decriptor = base.SelectAction(controllerContext);

      }

      return decriptor;

    }

  }



}
