﻿using api_abarrotes.InteractorMessages;
using api_abarrotes.ModelViews;
using api_abarrotes.UserStories.PopulationProfileHandler;
using api_abarrotes.Utils.GenericClass;
using api_abarrotes.Utils.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace api_abarrotes.Controllers
{
   [RoutePrefix("api/population-profiles")]
   public class PopulationProfileController : ApiBaseController
   {
      public IPopulationProfileInteractor GetInteractor()
      {
         return PopulationProfileInteractor.Create();
      }

      /// <summary>
      /// Get a list of population profiles for a region
      /// </summary>
      [Swagger.Net.Annotations.SwaggerExample("storeId", "123")]
      [Swagger.Net.Annotations.SwaggerExample("region", "Norte")]
      [Swagger.Net.Annotations.SwaggerResponse(HttpStatusCode.OK, Type = typeof(MVResponseBaseSuccessExample<MVGetPopulationProfilesResponse>))]
      [Authorize(Roles = "admin,user")]
      [HttpGet]
      [Route("")]
      public HttpResponseMessage GetPopulationProfiles([FromUri(Name = "")] MVGetPopulationProfilesRequest request)
      {
         //To-Do: request validation
         if (request is null) request = new MVGetPopulationProfilesRequest();


         using (var interactor = GetInteractor())
         {
            var response = interactor.GetPopulationProfiles(new GetPopulationProfilesRequest
            {
               StoreId = request.StoreId,
               Region = request.Region
            });

            var httpResponse = Request.CreateResponse((HttpStatusCode)response.StatusCode, response.Status ? MVResponseBase<MVGetPopulationProfilesResponse>.Create(new MVGetPopulationProfilesResponse()
            {
               Items = MapperHelper.Map<List<MVPopulationProfileItem>>(response.Data.Items),
               Total = response.Data.Total
            }) : MVResponseBase<MVGetPopulationProfilesResponse>.Create(response.Errors));

            return httpResponse;
         }
      }

   }
}
