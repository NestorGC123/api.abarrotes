﻿using api_abarrotes.InteractorMessages;
using api_abarrotes.ModelViews;
using api_abarrotes.UserStories.BestsellerHandler;
using api_abarrotes.Utils.GenericClass;
using api_abarrotes.Utils.Helpers;
using Swagger.Net.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace api_abarrotes.Controllers
{
  [RoutePrefix("api/bestsellers")]
  public class BestsellerController : ApiBaseController
  {
    public IBestsellerInteractor GetInteractor()
    {
      return BestsellerInteractor.Create();
    }

    /// <summary>
    /// Get a list of Bestsellers
    /// </summary>
    [Swagger.Net.Annotations.SwaggerExample("start", "0")]
    [Swagger.Net.Annotations.SwaggerExample("size", "10")]
    [Swagger.Net.Annotations.SwaggerExample("region", "Norte")]
    [Swagger.Net.Annotations.SwaggerExample("date", "2015-03-25T12:00:00Z")]
    [Swagger.Net.Annotations.SwaggerExample("storeId", "123")]
    [Swagger.Net.Annotations.SwaggerResponse(HttpStatusCode.OK, Type = typeof(MVResponseBaseSuccessExample<MVGetBestsellersResponse>))]
    [Authorize(Roles = "admin,user")]
    [HttpGet]
    [Route("")]
    public HttpResponseMessage GetBestSellers([FromUri(Name = "")] MVGetBestsellersRequest request)
    {
      //To-Do: request validation
      if (request is null) request = new MVGetBestsellersRequest();


      //return Request.CreateResponse((HttpStatusCode)200, request);
      using (var interactor = GetInteractor())
      {
        var response = interactor.GetBestsellers(new GetBestsellersRequest()
        {
          Size = request.Size,
          Start = request.Start,
          Region = request.Region,
          Date = request.Date,
          StoreId = request.StoreId
        });

        var httpResponse = Request.CreateResponse((HttpStatusCode)response.StatusCode, response.Status ? MVResponseBase<MVGetBestsellersResponse>.Create(new MVGetBestsellersResponse()
        {
          Items = MapperHelper.Map<List<MVBestsellerItem>>(response.Data.Items),
          Total = response.Data.Total
        }) : MVResponseBase<MVGetBestsellersResponse>.Create(response.Errors));

        httpResponse.ReasonPhrase = String.Join(",", response.Errors);

        return httpResponse;
      }
    }

  }
}
