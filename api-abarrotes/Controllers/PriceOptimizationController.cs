﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using api_abarrotes.InteractorMessages;
using api_abarrotes.ModelViews;
using api_abarrotes.UserStories.PriceOptimizationHandler;
using api_abarrotes.Utils.GenericClass;
using api_abarrotes.Utils.Helpers;

namespace api_abarrotes.Controllers
{
   [RoutePrefix("api/price-optimizations")]
   public class PriceOptimizationController : ApiBaseController
   {
      public IPriceOptimizationInteractor GetInteractor()
      {
         return PriceOptimizationInteractor.Create();
      }

      /// <summary>
      /// Get a list of product prices data for a region
      /// </summary>
      [Swagger.Net.Annotations.SwaggerExample("storeId", "123")]
      [Swagger.Net.Annotations.SwaggerExample("productName", "Atun")]
      [Swagger.Net.Annotations.SwaggerExample("region", "Norte")]
      [Swagger.Net.Annotations.SwaggerResponse(HttpStatusCode.OK, Type = typeof(MVResponseBaseSuccessExample<MVGetPriceOptimizationsResponse>))]
      [Authorize(Roles = "admin,user")]
      [HttpGet]
      [Route("")]
      public HttpResponseMessage GetPriceOptimizations([FromUri(Name = "")] MVGetPriceOptimizationsRequest request)
      {
         //To-Do: request validation
         if (request is null) request = new MVGetPriceOptimizationsRequest();


         using (var interactor = GetInteractor())
         {
            var response = interactor.GetPriceOptimizations(new GetPriceOptimizationsRequest
            {
               StoreId = request.StoreId,
               Region = request.Region,
               ProductName = request.ProductName
            });

            var httpResponse = Request.CreateResponse((HttpStatusCode)response.StatusCode, response.Status ? MVResponseBase<MVGetPriceOptimizationsResponse>.Create(new MVGetPriceOptimizationsResponse()
            {
               Items = MapperHelper.Map<List<MVPriceOptimizationItem>>(response.Data.Items),
               Total = response.Data.Total
            }) : MVResponseBase<MVGetPriceOptimizationsResponse>.Create(response.Errors));

            return httpResponse;
         }
      }

   }
}
