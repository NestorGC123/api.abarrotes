﻿using api_abarrotes.InteractorMessages;
using api_abarrotes.ModelViews;
using api_abarrotes.UserStories.SaleForecastHandler;
using api_abarrotes.Utils.GenericClass;
using api_abarrotes.Utils.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace api_abarrotes.Controllers
{
   [RoutePrefix("api/sale-forecasts")]
   public class SaleForecastController : ApiBaseController
    {
      public ISaleForecastInteractor GetInteractor()
      {
         return SaleForecastInteractor.Create();
      }


      /// <summary>
      /// Get a list of sale forecasts for a store
      /// </summary>
      [Swagger.Net.Annotations.SwaggerExample("storeId", "123")]
      [Swagger.Net.Annotations.SwaggerExample("fromDate", "2020-03-25T12:00:00Z")]
      [Swagger.Net.Annotations.SwaggerExample("untilDate", "2020-04-25T12:00:00Z")]
      [Swagger.Net.Annotations.SwaggerExample("aggregationType", "Semana")]
      [Swagger.Net.Annotations.SwaggerResponse(HttpStatusCode.OK, Type = typeof(MVResponseBaseSuccessExample<MVGetSaleForecastsResponse>))]
      [Authorize(Roles = "admin,user")]
      [HttpGet]
      [Route("")]
      public HttpResponseMessage GetInveForecasts([FromUri(Name = "")] MVGetSaleForecastsRequest request)
      {
         //To-Do: request validation
         if (request is null) request = new MVGetSaleForecastsRequest();


         using (var interactor = GetInteractor())
         {
            var response = interactor.GetSaleForecasts(new GetSaleForecastsRequest
            {
               StoreId = request.StoreId,
               AggregationType = request.AggregationType,
               FromDate = request.FromDate,
               UntilDate = request.UntilDate
            });

            var httpResponse = Request.CreateResponse((HttpStatusCode)response.StatusCode, response.Status ? MVResponseBase<MVGetSaleForecastsResponse>.Create(new MVGetSaleForecastsResponse()
            {
               Items = MapperHelper.Map<List<MVSaleForecastItem>>(response.Data.Items),
               Total = response.Data.Total
            }) : MVResponseBase<MVGetSaleForecastsResponse>.Create(response.Errors));

            httpResponse.ReasonPhrase = String.Join(",", response.Errors);

            return httpResponse;
         }
      }

   }
}
