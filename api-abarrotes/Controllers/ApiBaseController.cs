﻿using api_abarrotes.Utils.Cofigurations;
using System.Web.Http;
using System.Web.Http.Description;

namespace api_abarrotes.Controllers
{
  public class ApiBaseController : ApiController
  {
    //Aqui van configuraciones que aplicarian para todos los controladores
    public InteractorConfig INTER_CONFIG
    {
      get => new InteractorConfig
      {
        AlgoriteamDbConnection = GlobalConfigurations.ALGORITEAM_DB_CONNECTION,
        UserDbConnection = GlobalConfigurations.USER_DB_CONNECTION
      };
    }
  }
}
