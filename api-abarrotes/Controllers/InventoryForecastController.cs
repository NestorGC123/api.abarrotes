﻿using api_abarrotes.InteractorMessages;
using api_abarrotes.ModelViews;
using api_abarrotes.UserStories.InventoryForecastHandler;
using api_abarrotes.Utils.GenericClass;
using api_abarrotes.Utils.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace api_abarrotes.Controllers
{
  [RoutePrefix("api/inventory-forecasts")]
  public class InventoryForecastController : ApiBaseController
  {

    public IInventoryForecastInteractor GetInteractor()
    {
      return InventoryForecastInteractor.Create();
    }


    /// <summary>
    /// Get a list of product inventory forecasts for a store
    /// </summary>
    [Swagger.Net.Annotations.SwaggerExample("start", "0")]
    [Swagger.Net.Annotations.SwaggerExample("size", "10")]
    [Swagger.Net.Annotations.SwaggerExample("storeId", "123")]
    [Swagger.Net.Annotations.SwaggerExample("referenceDate", "2015-03-25T12:00:00Z")]
    [Swagger.Net.Annotations.SwaggerResponse(HttpStatusCode.OK, Type=typeof(MVResponseBaseSuccessExample<MVGetInventoryForecastsResponse>))]
    [Authorize(Roles = "admin,user")]
    [HttpGet]
    [Route("")]
    public HttpResponseMessage GetInventoryForecasts([FromUri(Name = "")] MVGetInventoryForecastsRequest request)
    {
      //To-Do: request validation
      if (request is null) request = new MVGetInventoryForecastsRequest();


      using (var interactor = GetInteractor())
      {
        var response = interactor.GetInventoryForecasts(new GetInventoryForecastsRequest
        {
          Size = request.Size,
          Start = request.Start,
          StoreId = request.StoreId,
          ReferenceDate = request.ReferenceDate
        });

        var httpResponse = Request.CreateResponse((HttpStatusCode)response.StatusCode, response.Status ? MVResponseBase<MVGetInventoryForecastsResponse>.Create(new MVGetInventoryForecastsResponse()
        {
          Items = MapperHelper.Map<List<MVInventoryForecastItem>>(response.Data.Items),
          Total = response.Data.Total
        }) : MVResponseBase<MVGetInventoryForecastsResponse>.Create(response.Errors));

        httpResponse.ReasonPhrase = String.Join(",", response.Errors);

        return httpResponse;
      }
    }

  }
}