﻿using api_abarrotes.InteractorMessages;
using api_abarrotes.ModelViews;
using api_abarrotes.UserStories.PromotionHandler;
using api_abarrotes.Utils.GenericClass;
using api_abarrotes.Utils.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace api_abarrotes.Controllers
{
   [RoutePrefix("api/promotions")]
   public class PromotionController : ApiBaseController
   {

      public IPromotionInteractor GetInteractor()
      {
         return PromotionInteractor.Create();
      }

      /// <summary>
      /// Get a list of promotions for a store
      /// </summary>
      [Swagger.Net.Annotations.SwaggerExample("storeId", "123")]
      [Swagger.Net.Annotations.SwaggerResponse(HttpStatusCode.OK, Type = typeof(MVResponseBaseSuccessExample<MVGetPromotionsResponse>))]
      [Authorize(Roles = "admin,user")]
      [HttpGet]
      [Route("")]
      public HttpResponseMessage GetPromotions([FromUri(Name = "")] MVGetPromotionsRequest request)
      {
         //To-Do: request validation
         if (request is null) request = new MVGetPromotionsRequest();


         using (var interactor = GetInteractor())
         {
            var response = interactor.GetPromotions(new GetPromotionsRequest
            {
               StoreId = request.StoreId
            });

            var httpResponse = Request.CreateResponse((HttpStatusCode)response.StatusCode, response.Status ? MVResponseBase<MVGetPromotionsResponse>.Create(new MVGetPromotionsResponse()
            {
               Items = MapperHelper.Map<List<MVPromotionItem>>(response.Data.Items),
               Total = response.Data.Total
            }) : MVResponseBase<MVGetPromotionsResponse>.Create(response.Errors));

            return httpResponse;
         }
      }


   }
}
