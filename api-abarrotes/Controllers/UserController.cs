﻿using api_abarrotes.InteractorMessages;
using api_abarrotes.ModelViews;
using api_abarrotes.Utils.Helpers;
using api_abarrotes.UserStories.UserHandler;
using api_abarrotes.Utils.GenericClass;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using Swagger.Net;
using System.Net.Http.Headers;

namespace api_abarrotes.Controllers
{
  [RoutePrefix("api/users")]
  public class UserController : ApiBaseController
  {

    public IUserInteractor GetInteractor()
    {
      return UserInteractor.Create(INTER_CONFIG);
    }

    [Authorize(Roles = "admin")]
    [HttpGet]
    [Route("")]
    public HttpResponseMessage GetAllUsers()
    {
      using (var interactor = GetInteractor())
      {
        var response = interactor.GetUsers(new GetUsersRequest()
        {
          //No hay ningun tipo de filtro
        });

        var httpResponse = Request.CreateResponse((HttpStatusCode)response.StatusCode, response.Status ? MVResponseBase<MVGetAllUsersResponse>.Create(new MVGetAllUsersResponse()
        {
          Items = MapperHelper.Map<List<MVUserItem>>(response.Data.Items),
          Total = response.Data.Total
        }) : MVResponseBase<MVGetAllUsersResponse>.Create(response.Errors));

        //httpResponse.ReasonPhrase = String.Join(",", response.Errors);

        return httpResponse;
      }
    }

    [Authorize(Roles = "admin")]
    [HttpGet]
    [Route("current")]
    public HttpResponseMessage GetCurrentUser()
    {
      using (var interactor = GetInteractor())
      {
        var response = interactor.GetUser(new GetUserRequest()
        {
          UserId = User.Identity.GetUserId()
        });

        var httpResponse = Request.CreateResponse((HttpStatusCode)response.StatusCode, response.Status ? MVResponseBase<MVGetCurrentUserResponse>.Create(new MVGetCurrentUserResponse()
        {
          Item = MapperHelper.Map<MVUserItem>(response.Data.Item)
        }) : MVResponseBase<MVGetCurrentUserResponse>.Create(response.Errors));

        //httpResponse.ReasonPhrase = String.Join(",", response.Errors);

        return httpResponse;
      }
    }
  }
}
