﻿using Microsoft.Owin.Security.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace api_abarrotes.Providers
{
  public class OAuthRefreshProvider : AuthenticationTokenProvider
  {
    public override void Create(AuthenticationTokenCreateContext context)
    {
      context.Ticket.Properties.ExpiresUtc = new DateTimeOffset(DateTime.Now.AddDays(1));
      context.SetToken(context.SerializeTicket());
    }

    public override void Receive(AuthenticationTokenReceiveContext context)
    {
      context.DeserializeTicket(context.Token);
    }
  }
}