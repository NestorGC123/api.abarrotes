﻿using api_abarrotes.User.Db.IdentityManager;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using UserEntity = api_abarrotes.User.Entities.User;

namespace api_abarrotes.Providers
{
  public class OAuthProvider : OAuthAuthorizationServerProvider
  {
    //private readonly string _publicClientId;
    //Segun yo esto no es necesario, investigar
    //public OAuthProvider(string publicClientId)
    //{
    //  if (publicClientId == null)
    //  {
    //    throw new ArgumentNullException("publicClientId");
    //  }

    //  _publicClientId = publicClientId;
    //}

    // Metodo para validar el cliente que intenta autenticarse
    public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
    {
      // La credenciales de la contraseña del propietario del recurso no proporcionan un id. de cliente.
      //if (context.ClientId == null)
      //{
        context.Validated();
      //}

      //return Task.FromResult<object>(null);
    }

    // Metodo para validar las credenciales del usuario y generarle un token
    public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
    {
      var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();

      UserEntity user = await userManager.FindAsync(context.UserName, context.Password);

      if (user == null)
      {
        context.SetError("invalid_grant", "El nombre de usuario o la contraseña no son correctos");
        return;
      }

      ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(userManager,
         OAuthDefaults.AuthenticationType);

      //Generamos respuesta adicional en el JWT
      var roles = userManager.GetRoles(user.Id);
      var roles_str = roles.Aggregate((a, b) => a + ", " + b);

      var name = string.Format("{0}", user.UserName);

      AuthenticationProperties properties = CreateProperties(name, roles_str);
      AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, properties); //Genera el JWT completo
      context.Validated(ticket);
    }

    public override async Task TokenEndpoint(OAuthTokenEndpointContext context)
    {
      foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
      {
        context.AdditionalResponseParameters.Add(property.Key, property.Value);
      }

    }


    public static AuthenticationProperties CreateProperties(string name, string rol)
    {
      IDictionary<string, string> data = new Dictionary<string, string>
            {
                { "user", name },
                { "rol", rol },
            };
      return new AuthenticationProperties(data);
    }

  }
}