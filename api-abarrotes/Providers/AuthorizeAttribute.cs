﻿using api_abarrotes.Utils.GenericClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;

namespace api_abarrotes
{
  public class AuthorizeAttribute : System.Web.Http.AuthorizeAttribute
  {
    protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
    {
      if (HttpContext.Current.User.Identity.IsAuthenticated)
      {
        var model = MVResponseBase<Object>.Create(new List<string> { "Authorization is required to access this endpoint" });
        actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Forbidden, model);
      }
      else
      {
        var model = MVResponseBase<Object>.Create(new List<string> { "Authentication is required to access this endpoint" });
        actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, model);
      }
    }
  }
}