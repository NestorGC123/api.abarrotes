﻿using System;
using System.Threading.Tasks;
using api_abarrotes.Providers;
using api_abarrotes.User.Db.Context;
using api_abarrotes.User.Db.IdentityManager;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;

[assembly: OwinStartup(typeof(api_abarrotes.Startup))]

namespace api_abarrotes
{
  public class Startup
  {
    public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }
    //public static string PublicClientId { get; private set; }

    public void Configuration(IAppBuilder app)
    {
      // Configure the db context, user manager and signin manager to use a single instance per request
      app.CreatePerOwinContext(UserDbContext.Create);
      app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
      //app.CreatePerOwinContext<ApplicationSignInManager>(ApplicationSignInManager.Create);

      //app.UseCors(); xD


      //app.UseCors(CorsOptions.AllowAll);


      //app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
      //PublicClientId = "self";
      OAuthOptions = new OAuthAuthorizationServerOptions
      {
        TokenEndpointPath = new PathString("/api/auth/token"),
        Provider = new OAuthProvider(),
        //AuthorizeEndpointPath = new PathString("/api/Account/ExternalLogin"),
        AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
        RefreshTokenProvider = new OAuthRefreshProvider(),
        //RefreshTokenProvider = new ApplicationOAuthRefreshProvider(),
        // In production mode set AllowInsecureHttp = false
        AllowInsecureHttp = true
      };

      //// Enable the application to use bearer tokens to authenticate users
      app.UseOAuthBearerTokens(OAuthOptions);
    }
  }
}
