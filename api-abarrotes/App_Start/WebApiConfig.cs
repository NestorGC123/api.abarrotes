﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web.Http;

namespace api_abarrotes
{
  public static class WebApiConfig
  {
    public static void Register(HttpConfiguration config)
    {
      // Configuración y servicios de API web
      // Cors
      
      //Not cors, don't want it
      //var cors = new EnableCorsAttribute("*", "*", "*");
      //config.EnableCors(cors);
      
      //config.EnableCors();

      // Rutas de API web
      config.MapHttpAttributeRoutes();

      //config.Routes.MapHttpRoute(
      //    name: "DefaultApi",
      //    routeTemplate: "api/{controller}/{action}/{id}",
      //    defaults: new { id = RouteParameter.Optional }
      //);

      config.Routes.MapHttpRoute(
        name: "NotFound",
        routeTemplate: "{*url}",
        defaults: new { controller = "NotFound", action = "Handle404" }
      );

      // Formatters
      //config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
      config.Formatters.XmlFormatter.SupportedMediaTypes.Clear();
      config.Formatters.JsonFormatter.SerializerSettings.Formatting = Newtonsoft.Json.Formatting.Indented;


    }
  }
}
