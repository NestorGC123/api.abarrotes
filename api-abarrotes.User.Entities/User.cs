﻿using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Claims;

namespace api_abarrotes.User.Entities
{
  public class User : IdentityUser<string,UserLogin, UserRole, UserClaim>
  {
    #region audit
    public int? Status { get; set; }
    public DateTime? CreateDate { get; set; }
    public DateTime? UpdateDate { get; set; }
    public string CreateBy { get; set; }
    public string UpdateBy { get; set; }
    #endregion

    public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User, string> manager, string authenticationType)
    {
      var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
      return userIdentity;
    }

  }
}
