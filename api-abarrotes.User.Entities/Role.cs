﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.User.Entities
{
  public class Role : IdentityRole<string, UserRole>
  {
    public Role() { }
    public Role(string name) { Name = name; }

    public int? Status { get; set; }
    public DateTime? CreateDate { get; set; }
    public DateTime? UpdateDate { get; set; }
    public string CreateBy { get; set; }
    public string UpdateBy { get; set; }
  }
}
