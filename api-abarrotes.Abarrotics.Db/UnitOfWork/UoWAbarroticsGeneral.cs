﻿using api_abarrotes.Abarrotics.Db.Context;
using api_abarrotes.Abarrotics.Entities;
using api_abarrotes.Utils.OdbcProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.Abarrotics.Db.UnitOfWork
{
   public class UoWAbarroticsGeneral : IDisposable
   {
      private bool Disposed = false;
      private AbarroticsDbContext Context { get; set; }
      public OdbcDbSet<Sale> SaleDbSet { get; set; }

      public static UoWAbarroticsGeneral Create(string connection) => new UoWAbarroticsGeneral(connection);
      public UoWAbarroticsGeneral(string connection)
      {
         Context = AbarroticsDbContext.Create(connection);
         SaleDbSet = Context.Sales;
      }

      protected virtual void Dispose(bool Disposing)
      {
         if (!this.Disposed)
         {
            if (Disposing)
            {
               Context.Dispose();
            }
         }
         Disposed = true;
      }
      public void Dispose()
      {
         Dispose(true);
         GC.SuppressFinalize(this);
      }
   }
}
