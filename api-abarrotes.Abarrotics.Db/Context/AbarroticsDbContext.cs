﻿using api_abarrotes.Abarrotics.Entities;
using api_abarrotes.Utils.OdbcProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.Abarrotics.Db.Context
{
   public class AbarroticsDbContext : OdbcDbContext
   {
      public static AbarroticsDbContext Create(string connectionStringName) => new AbarroticsDbContext(connectionStringName);
      public AbarroticsDbContext(string connectionStringName) : base(connectionStringName) { }

      public OdbcDbSet<Sale> Sales { get; private set; }

      protected override void initializeOdbcDbSets()
      {
         var salePropertyFieldMaps = new List<OdbcPropertyFieldMap>()
         {
            new OdbcPropertyFieldMap()
            {
               PropertyName = nameof(Sale.StoreId),
               FieldName = "store_id"
            },
            new OdbcPropertyFieldMap()
            {
               PropertyName = nameof(Sale.Id),
               FieldName = "id"
            },
            new OdbcPropertyFieldMap()
            {
               PropertyName = nameof(Sale.ExchangeRate),
               FieldName = "exchange_rate"
            }, 
            new OdbcPropertyFieldMap()
            {
               PropertyName = nameof(Sale.ChangeAmount),
               FieldName = "change_amount"
            },
            new OdbcPropertyFieldMap()
            {
               PropertyName = nameof(Sale.Currency),
               FieldName = "currency"
            },
            new OdbcPropertyFieldMap()
            {
               PropertyName = nameof(Sale.Date),
               FieldName = "date"
            },
            new OdbcPropertyFieldMap()
            {
               PropertyName = nameof(Sale.PaymentType),
               FieldName = "payment_type"
            },
            new OdbcPropertyFieldMap()
            {
               PropertyName = nameof(Sale.Status),
               FieldName = "status"
            },
            new OdbcPropertyFieldMap()
            {
               PropertyName = nameof(Sale.Tax),
               FieldName = "tax"
            },
            new OdbcPropertyFieldMap()
            {
               PropertyName = nameof(Sale.Total),
               FieldName = "total"
            }
         };
         Sales = new OdbcDbSet<Sale>(this, "sales", salePropertyFieldMaps);
      }
   }
}
