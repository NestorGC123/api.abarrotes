﻿using api_abarrotes.InteractorMessages;
using api_abarrotes.Utils.GenericClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.UserStories.PriceOptimizationHandler
{
   public interface IPriceOptimizationInteractor : IDisposable
   {
      MsgResponseBase<GetPriceOptimizationsResponse> GetPriceOptimizations(GetPriceOptimizationsRequest request);
   }
}
