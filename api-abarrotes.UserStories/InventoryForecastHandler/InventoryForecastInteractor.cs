﻿using api_abarrotes.Data.Db.UnitOfWork;
using api_abarrotes.Data.Entities;
using api_abarrotes.InteractorMessages;
using api_abarrotes.Utils.Cofigurations;
using api_abarrotes.Utils.GenericClass;
using api_abarrotes.Utils.Helpers;
using Bogus;
using Bogus.DataSets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.UserStories.InventoryForecastHandler
{
   public class InventoryForecastInteractor : IInventoryForecastInteractor
   {
      private UoWAlgoriteamGeneral uoWForecast { get; set; }

      public static InventoryForecastInteractor Create() => new InventoryForecastInteractor();
      public InventoryForecastInteractor()
      {
         uoWForecast = UoWAlgoriteamGeneral.Create(
             connection: GlobalConfigurations.ALGORITEAM_DB_CONNECTION);
         //uoWCatalogs = UoWCatalogs.Create(connection: config.DataDbConnection);
      }   
      
      public MsgResponseBase<GetInventoryForecastsResponse> GetInventoryForecasts(GetInventoryForecastsRequest request)
      {

         //This implementation is just mock data
         //try
         //{
         //   string[] ProductNames = new string[] 
         //   {
         //      "COCA COLA 2,5L RETORNABLE", 
         //      "ACEITE NUTRIOLI 400ml", 
         //      "JABON ZOTE 25/400G BLANCO",  
         //      "GALLETAS GLOBITOS 100G",
         //      "PAN FRANCES",
         //      "AVENA RIVERO 1KG",
         //      "PAN BLANCO BIMBO GRANDE 680G",
         //      "PAPAS SABRITAS 67G",
         //      "PAPAS SABRITAS RUFFLES QUESO 280G",
         //      "GALLETA GAMESA EMPERADOR CHOCOLATE 91G",
         //      "GALLETA GAMESE EMPERADOR VAINILLA 91G",
         //      "TOALLAS INTIMA NATURELA MANZANILLA 8PIEZAS"
         //   };

         //   Randomizer.Seed = new Random(123456789);
         //   var inventoryForecatFaker = new Faker<InventoryForecastItem>()
         //     .RuleFor(x => x.ProductId, x => x.Random.Guid().ToString())
         //     //.RuleFor(x => x.ProductName, x => x.Commerce.ProductName())
         //     .RuleFor(x => x.InventoryForecast, x => x.Random.Number(1, 25));

         //   //Calculate week of the year
         //   int dayOfTheYear = request.ReferenceDate.DayOfYear;
         //   int dayOfTheMonth = request.ReferenceDate.Day;
         //   int monthOfTheYear = request.ReferenceDate.Month;

         //   int weekOfTheYear = (int)Math.Ceiling(dayOfTheYear / 7.0);

         //   int validFromDay = ((weekOfTheYear - 1) * 7) + 1;
         //   int validFromDayOfTheMonth = dayOfTheMonth - (dayOfTheYear - validFromDay);
         //   int validFromMonth = validFromDayOfTheMonth < 0 ? monthOfTheYear - 1 : monthOfTheYear;
         //   int validFromYear = request.ReferenceDate.Year;
         //   if (validFromMonth < 0)
         //   {
         //      validFromMonth = 12;
         //      validFromYear--;
         //   }
         //   DateTime validFromDate = new DateTime(validFromYear, validFromMonth, validFromDayOfTheMonth, 0, 0, 0, DateTimeKind.Utc);
         //   DateTime validUntilDate = validFromDate.AddDays(7).AddHours(24);

         //   var inventoryForecastMockItems = new List<InventoryForecastItem>();
         //   for (int i = 0; i < 10; i++)
         //   {
         //      var fake = inventoryForecatFaker.Generate();
         //      fake.ProductName = ProductNames[i];
         //      fake.ValidFromDate = validFromDate;
         //      fake.ValidUntilDate = validUntilDate;
         //      inventoryForecastMockItems.Add(fake);
         //   }

         //   var total = inventoryForecastMockItems.Count();
         //   var inventoryForecastItems = inventoryForecastMockItems.Skip(request.Start).Take(request.Size).ToList();

         //   return MsgResponseBase<GetInventoryForecastsResponse>.Create(new GetInventoryForecastsResponse
         //   {
         //      Total = total,
         //      Items = inventoryForecastItems
         //   }, 200);

         //}
         //catch (Exception e)
         //{
         //   return MsgResponseBase<GetInventoryForecastsResponse>.Create(new List<string> { e.Message }, 500);
         //}

         try
         {
            int weekOfTheYear = (int)Math.Ceiling(request.ReferenceDate.DayOfYear / 7.0);

            List<PrediccionInventario> entities;

            if (request.ReferenceDate != null)
               entities = uoWForecast.PredicionInventarioDbSet.Get(offset: request.Start, limit: request.Size, where: String.Format("{0} == {1} AND {2} == {3}", nameof(PrediccionInventario.Semana), weekOfTheYear, nameof(PrediccionInventario.Year), request.ReferenceDate.Year));
            else
               entities = uoWForecast.PredicionInventarioDbSet.Get(offset: request.Start, limit: request.Size);

            int total = uoWForecast.PredicionInventarioDbSet.Count();

            List<InventoryForecastItem> inventoryForecastItems = new List<InventoryForecastItem>();

            foreach (PrediccionInventario entity in entities)
            {
               var item = new InventoryForecastItem()
               {
                  ProductId = entity.ProductId,
                  ProductName = entity.ProductName,
                  InventoryForecast = entity.Prediction
               };

               int? month;
               int? day;
               int daysInTheYear = DateTime.IsLeapYear(entity.Year) ? 366 : 365;

               //Start day of the year for this week
               int validFromDayOfTheYear = ((entity.Semana - 1) * 7) + 1;
               CalculateMonthAndDay(entity.Year, validFromDayOfTheYear, out month, out day);
               item.ValidFromDate = new DateTime(entity.Year, month.Value, day.Value, 0, 0, 0, DateTimeKind.Utc);

               //End day of the year for this week
               int validUntilDayOfTheYear = entity.Semana * 7;
               if (validUntilDayOfTheYear > daysInTheYear) validUntilDayOfTheYear = daysInTheYear;
               CalculateMonthAndDay(entity.Year, validUntilDayOfTheYear, out month, out day);
               item.ValidUntilDate = new DateTime(entity.Year, month.Value, day.Value, 0, 0, 0, DateTimeKind.Utc);

               inventoryForecastItems.Add(item);
            }


            return MsgResponseBase<GetInventoryForecastsResponse>.Create(new GetInventoryForecastsResponse
            {
               Total = total,
               Items = inventoryForecastItems
            }, 200);
         }
         catch (Exception e)
         {
            return MsgResponseBase<GetInventoryForecastsResponse>.Create(new List<string> { e.Message }, 500);
         }
      }

      private void CalculateMonthAndDay(int year, int dayOfTheYear, out int? month, out int? day)
      {
         month = null;
         day = null;

         int daysSummatory = 0;
         foreach (int i in new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 })
         {
            daysSummatory += DateTime.DaysInMonth(year, i);
            if (daysSummatory >= dayOfTheYear)
            {
               month = i;
               break;
            }
         }

         day = dayOfTheYear - (daysSummatory - DateTime.DaysInMonth(year, month.Value));
      }

      public void Dispose()
      {
         //throw new NotImplementedException();
      }
   }
}
