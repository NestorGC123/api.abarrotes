﻿using api_abarrotes.InteractorMessages;
using api_abarrotes.Utils.GenericClass;
using Bogus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.UserStories.PromotionHandler
{
   public class PromotionInteractor : IPromotionInteractor
   {
      public static PromotionInteractor Create() => new PromotionInteractor();
      public PromotionInteractor()
      {
         //uoWUser = UoWUser.Create(
         //    connection: config.UserDbConnection);
         //uoWCatalogs = UoWCatalogs.Create(connection: config.DataDbConnection);
      }
      public MsgResponseBase<GetPromotionsResponse> GetPromotions(GetPromotionsRequest request)
      {
         //This implementation is just mock data
         try
         {
            string[] ProductNames = new string[]
            {
               "TOSTITOS SALSA VERDE 240G",
               "PAPAS SABRITAS RUFFLES QUESO 280G",
               "GALLETA GAMESA EMPERADOR CHOCOLATE 91G",
               "SOPA INSTANTANEA MARUCHAN CON CAMARON 64G",
               "COCA COLA 2,5L RETORNABLE",
               "ACEITE NUTRIOLI 400ml",
               "DETERGENTE ACE 100G",
               "AGUA EPURA 500ml",
               "JABON ZOTE 25/400G BLANCO",
               "GALLETAS GLOBITOS 100G",
               "DORITOS NACHO QUESO Y JALAPEÑO 67G",
               "SABRITAS LIGHT 67G",
               "AVENA RIVERO 1KG",
               "PAN BLANCO BIMBO GRANDE 680G",
               "PAPAS SABRITAS 67G",
               "GALLETA GAMESE EMPERADOR VAINILLA 91G",
               "TOALLAS INTIMA NATURELA MANZANILLA 8PIEZAS",
               "DORITOS NACHO QUESO Y JALAPEÑO 67G",
               "SABRITAS LIGHT 67G",
               "AVENA RIVERO 1KG",
               "PAN BLANCO BIMBO GRANDE 680G",
               "PAPAS SABRITAS 67G",
               "COCA COLA 2,5L RETORNABLE",
               "GALLETA GAMESE EMPERADOR VAINILLA 91G",
               "TOALLAS INTIMA NATURELA MANZANILLA 8PIEZAS",
               "TOSTITOS SALSA VERDE 240G",
               "PAPAS SABRITAS RUFFLES QUESO 280G",
               "GALLETA GAMESA EMPERADOR CHOCOLATE 91G",
               "SOPA INSTANTANEA MARUCHAN CON CAMARON 64G",
               "ACEITE NUTRIOLI 400ml",
               "DETERGENTE ACE 100G",
               "AGUA EPURA 500ml",
               "JABON ZOTE 25/400G BLANCO",
               "GALLETAS GLOBITOS 100G",
               "GALLETAS MARIAS GAMESA 144G",
               "GALLETAS DONDE ANIMALITOS 158G",
               "GALLETAS DONDE GLOBITOS 100G",
               "PAN BLANCO BIMBO GRANDE 680G",
               "CHARRITOS LA LUPITA CON CHILE 330G",
               "PALETA HOLANDA MAGNUM 110ml",
               "PAPAS SABRITAS 67G",
               "COCA COLA 2,5L RETORNABLE",
               "SOPA INSTANTANEA MARUCHAN CON CAMARON 64G",
               "TOSTITOS SALSA VERDE 240G",
               "PAPAS SABRITAS RUFFLES QUESO 280G",
               "DETERGENTE ACE 100G",
               "AGUA EPURA 500ml",
               "MAYONESA MCCORMIK 775G",
              "CHILES JALAPEÑOS LA CONSTEÑA ENTEROS EN ESCABECHE 380G",
              "FRIJOLES BAYOS LA SIERRA REFRITOS EN BOLSA 430G",
              "SOPA DE PLUMA ITALPASTA GRANDE 200G",
              "SPAGHETTI ITALPASTA 200G",
              "PAPAS SABRITAS 67G",
                             "AGUA EPURA 500ml",
                                            "GALLETAS DONDE GLOBITOS 100G",
               "COCA COLA 2,5L RETORNABLE",
               "COCA COLA 500ml DESECHABLE",
               "GALLETAS MARIAS GAMESA 144G",
               "GALLETAS DONDE ANIMALITOS 158G",
               "PAN BLANCO BIMBO GRANDE 680G",
               "CHARRITOS LA LUPITA CON CHILE 330G",
               "PALETA HOLANDA MAGNUM 110ml",

               "COCA COLA 2,5L RETORNABLE",
               "SOPA INSTANTANEA MARUCHAN CON CAMARON 64G",
               "TOSTITOS SALSA VERDE 240G",
               "PAPAS SABRITAS RUFFLES QUESO 280G",
               "DETERGENTE ACE 100G",
               "LECHE EN POLVO NIDO CLASICA 1.56KG",
               "CHOCOLATE ABUELITA TABLILLAS 540G",
               "PAPILLA GERBER DE MANZANA 113G",
               "MAZAPAN DE CACAHUETE DE LA ROSA 28G",
               "TALCO MENNEN PARA BEBE 200G",
               "CHEETOS POFFS 170G",
               "BIMBO MEDIAS NOCHES 8 PZAS",
               "AGUA MINERAL PEÑAFIEL 2L",
                              "DETERGENTE ACE 100G",
                                             "COCA COLA 2,5L RETORNABLE",
            };

            Randomizer.Seed = new Random(123456789);
            var promotionFaker = new Faker<PromotionItem>("es_MX")
              .RuleFor(x => x.PromotionId, x => x.Random.Guid().ToString())
              .RuleFor(x => x.PromotionCost, x => String.Format("{0:0.00}", x.Random.Double(30, 100)))
              .RuleFor(x => x.Products, x => new List<PromotionProductItem>());

            var promotionProductFaker = new Faker<PromotionProductItem>("es_MX")
               .RuleFor(x => x.ProductId, x => x.Random.Guid().ToString())
               .RuleFor(x => x.ProductName, x => x.PickRandom(ProductNames))
               .RuleFor(x => x.ProductQuantity, x => 1);

            var promotionMockItems = new List<PromotionItem>();
            //while (promotionMockItems.Count() < 10)
            for (int i = 0; i < 10; i++)
            {
               var promo = promotionFaker.Generate();

               do
               {
                  promo.Products.Clear();

                  for (int numProd = 0; numProd < 2; numProd++)
                  {
                     if (promo.Products.Count() > 0)
                     {
                        var product = promotionProductFaker.Generate();
                        while (product.ProductName.ToLower() == promo.Products[0].ProductName.ToLower())
                           product = promotionProductFaker.Generate();

                        promo.Products.Add(product);
                     }
                     else
                     {
                        promo.Products.Add(promotionProductFaker.Generate());
                     }
                  }

                  promo.Description = string.Join(" & ", promo.Products.Select(p => p.ProductName));
               }
               while (promotionMockItems.Select(pmi => pmi.Description.Contains(promo.Products[0].ProductName) && pmi.Description.Contains(promo.Products[1].ProductName)).Count( r => r == true) != 0);
               
               promotionMockItems.Add(promo);
            }

            var total = promotionMockItems.Count();
            var promotionItems = promotionMockItems.ToList();

            return MsgResponseBase<GetPromotionsResponse>.Create(new GetPromotionsResponse
            {
               Total = total,
               Items = promotionItems
            }, 200);

         }
         catch (Exception e)
         {
            return MsgResponseBase<GetPromotionsResponse>.Create(new List<string> { e.Message }, 500);
         }
      }

      public void Dispose()
      {
      }
   }
}
