﻿using api_abarrotes.InteractorMessages;
using api_abarrotes.Utils.GenericClass;
using Bogus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.UserStories.BestsellerHandler
{
   public class BestsellerInteractor : IBestsellerInteractor
   {

      public static BestsellerInteractor Create() => new BestsellerInteractor();
      public BestsellerInteractor()
      {
         //uoWUser = UoWUser.Create(
         //    connection: config.UserDbConnection);
         //uoWCatalogs = UoWCatalogs.Create(connection: config.DataDbConnection);
      }

      public void Dispose()
      {

      }


      public MsgResponseBase<GetBestsellersResponse> GetBestsellers(GetBestsellersRequest request)
      {

         //This implementation is just mock data
         try
         {
            string[] ProductNamesCentro = new string[]
            {
               "TOSTITOS SALSA VERDE 240G",
               "PAPAS SABRITAS RUFFLES QUESO 280G",
               "GALLETA GAMESA EMPERADOR CHOCOLATE 91G",
               "SOPA INSTANTANEA MARUCHAN CON CAMARON 64G",
               "COCA COLA 2,5L RETORNABLE",
               "ACEITE NUTRIOLI 400ml",
               "DETERGENTE ACE 100G",
               "AGUA EPURA 500ml",
               "JABON ZOTE 25/400G BLANCO",
               "GALLETAS GLOBITOS 100G",
               "DORITOS NACHO QUESO Y JALAPEÑO 67G",
               "SABRITAS LIGHT 67G",
               "AVENA RIVERO 1KG",
               "PAN BLANCO BIMBO GRANDE 680G",
               "PAPAS SABRITAS 67G",
               "GALLETA GAMESE EMPERADOR VAINILLA 91G",
               "TOALLAS INTIMA NATURELA MANZANILLA 8PIEZAS"
            };

            string[] ProductNamesNorte = new string[]
            {
               "DORITOS NACHO QUESO Y JALAPEÑO 67G",
               "SABRITAS LIGHT 67G",
               "AVENA RIVERO 1KG",
               "PAN BLANCO BIMBO GRANDE 680G",
               "PAPAS SABRITAS 67G",
               "COCA COLA 2,5L RETORNABLE",
               "GALLETA GAMESE EMPERADOR VAINILLA 91G",
               "TOALLAS INTIMA NATURELA MANZANILLA 8PIEZAS",
               "TOSTITOS SALSA VERDE 240G",
               "PAPAS SABRITAS RUFFLES QUESO 280G",
               "GALLETA GAMESA EMPERADOR CHOCOLATE 91G",
               "SOPA INSTANTANEA MARUCHAN CON CAMARON 64G",
               "ACEITE NUTRIOLI 400ml",
               "DETERGENTE ACE 100G",
               "AGUA EPURA 500ml",
               "JABON ZOTE 25/400G BLANCO",
               "GALLETAS GLOBITOS 100G"
            };

            string[] ProductNamesSur = new string[]
            {
               "GALLETAS MARIAS GAMESA 144G",
               "GALLETAS DONDE ANIMALITOS 158G",
               "GALLETAS DONDE GLOBITOS 100G",
               "PAN BLANCO BIMBO GRANDE 680G",
               "CHARRITOS LA LUPITA CON CHILE 330G",
               "PALETA HOLANDA MAGNUM 110ml",
               "PAPAS SABRITAS 67G",
               "COCA COLA 2,5L RETORNABLE",
               "SOPA INSTANTANEA MARUCHAN CON CAMARON 64G",
               "TOSTITOS SALSA VERDE 240G",
               "PAPAS SABRITAS RUFFLES QUESO 280G",
               "DETERGENTE ACE 100G",
               "AGUA EPURA 500ml",
            };

            string[] ProductNamesPoniente = new string[]
           {
              "MAYONESA MCCORMIK 775G",
              "CHILES JALAPEÑOS LA CONSTEÑA ENTEROS EN ESCABECHE 380G",
              "FRIJOLES BAYOS LA SIERRA REFRITOS EN BOLSA 430G",
              "SOPA DE PLUMA ITALPASTA GRANDE 200G",
              "SPAGHETTI ITALPASTA 200G",
              "PAPAS SABRITAS 67G",
                             "AGUA EPURA 500ml",
                                            "GALLETAS DONDE GLOBITOS 100G",
               "COCA COLA 2,5L RETORNABLE",
               "COCA COLA 500ml DESECHABLE",
               "GALLETAS MARIAS GAMESA 144G",
               "GALLETAS DONDE ANIMALITOS 158G",
               "PAN BLANCO BIMBO GRANDE 680G",
               "CHARRITOS LA LUPITA CON CHILE 330G",
               "PALETA HOLANDA MAGNUM 110ml",

               "COCA COLA 2,5L RETORNABLE",
               "SOPA INSTANTANEA MARUCHAN CON CAMARON 64G",
               "TOSTITOS SALSA VERDE 240G",
               "PAPAS SABRITAS RUFFLES QUESO 280G",
               "DETERGENTE ACE 100G",
           };

            string[] ProductNamesOriente = new string[]
            {
               "LECHE EN POLVO NIDO CLASICA 1.56KG",
               "CHOCOLATE ABUELITA TABLILLAS 540G",
               "PAPILLA GERBER DE MANZANA 113G",
               "MAZAPAN DE CACAHUETE DE LA ROSA 28G",
               "TALCO MENNEN PARA BEBE 200G",
               "CHEETOS POFFS 170G",
               "BIMBO MEDIAS NOCHES 8 PZAS",
               "AGUA MINERAL PEÑAFIEL 2L",
                              "DETERGENTE ACE 100G",
                                             "COCA COLA 2,5L RETORNABLE",


            };

            Randomizer.Seed = new Random(123456789);
            var bestsellerFaker = new Faker<BestsellerItem>()
              //.RuleFor(x => x.Name, x => x.Commerce.ProductName())
              .RuleFor(x => x.Price, x => x.Commerce.Price());

            var bestsellerMockItems = new List<BestsellerItem>();
            for (int i = 0; i < 10; i++)
            {
               var bestSeller = bestsellerFaker.Generate();

               if (request.Region.ToLower() == "centro")
                  bestSeller.Name = ProductNamesCentro[i];
               if (request.Region.ToLower() == "norte")
                  bestSeller.Name = ProductNamesNorte[i];
               if (request.Region.ToLower() == "sur")
                  bestSeller.Name = ProductNamesSur[i];
               if (request.Region.ToLower() == "poniente")
                  bestSeller.Name = ProductNamesPoniente[i];
               if (request.Region.ToLower() == "oriente")
                  bestSeller.Name = ProductNamesOriente[i];
               if (bestSeller.Name == null)
                  bestSeller.Name = ProductNamesCentro[i];
               bestsellerMockItems.Add(bestSeller);
            }

            var total = bestsellerMockItems.Count();
            var bestsellerItems = bestsellerMockItems.Skip(request.Start).Take(request.Size).ToList();

            return MsgResponseBase<GetBestsellersResponse>.Create(new GetBestsellersResponse
            {
               Total = total,
               Items = bestsellerItems
            }, 200);

         }
         catch (Exception e)
         {
            return MsgResponseBase<GetBestsellersResponse>.Create(new List<string> { e.Message }, 500);
         }
      }
   }
}
