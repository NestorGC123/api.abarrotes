﻿using api_abarrotes.InteractorMessages;
using api_abarrotes.Utils.GenericClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.UserStories.BestsellerHandler
{
  public interface IBestsellerInteractor: IDisposable
  {
    MsgResponseBase<GetBestsellersResponse> GetBestsellers(GetBestsellersRequest request);
  }
}
