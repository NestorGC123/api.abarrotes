﻿using api_abarrotes.InteractorMessages;
using api_abarrotes.Utils.GenericClass;
using Bogus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.UserStories.PopulationProfileHandler
{
   public class PopulationProfileInteractor : IPopulationProfileInteractor
   {
      public static PopulationProfileInteractor Create() => new PopulationProfileInteractor();
      public PopulationProfileInteractor()
      {
         //uoWUser = UoWUser.Create(
         //    connection: config.UserDbConnection);
         //uoWCatalogs = UoWCatalogs.Create(connection: config.DataDbConnection);
      }


      public MsgResponseBase<GetPopulationProfilesResponse> GetPopulationProfiles(GetPopulationProfilesRequest request)
      {
         //This implementation is just mock data
         try
         {

            Randomizer.Seed = new Random(123456789);
            var populationProfileFaker = new Faker<PopulationProfileItem>("es_MX")
              .RuleFor(x => x.NumberOfPeople, x => x.Random.Int(1000, 3000));


            var populationProfileMockItems = new List<PopulationProfileItem>();
            var profiles = new List<string>() { "De 0 a 14 años", "De 15 a 29 años", "De 30 a 59 años", "De 60 y más años" };
            foreach(var profile in profiles)
            {
               var profileAux = populationProfileFaker.Generate();
               profileAux.AgeRange = profile;

               populationProfileMockItems.Add(profileAux);
            }

            var totalPopulation = populationProfileMockItems.Sum(pp => pp.NumberOfPeople);
            foreach(var populationProfile in populationProfileMockItems)
            {
               var percentage = ( (double) populationProfile.NumberOfPeople / totalPopulation) * 100;
               populationProfile.PercentageOfPeople = String.Format("{0:0.00}", percentage);
            }

            var total = populationProfileMockItems.Count();
            var populationProfileItems = populationProfileMockItems.ToList();

            return MsgResponseBase<GetPopulationProfilesResponse>.Create(new GetPopulationProfilesResponse
            {
               Total = total,
               Items = populationProfileItems
            }, 200);

         }
         catch (Exception e)
         {
            return MsgResponseBase<GetPopulationProfilesResponse>.Create(new List<string> { e.Message }, 500);
         }
      }

      public void Dispose()
      {

      }
   }
}
