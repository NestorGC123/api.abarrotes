﻿using api_abarrotes.Utils.GenericClass;
using api_abarrotes.InteractorMessages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.UserStories.UserHandler
{
  public interface IUserInteractor : IDisposable
  {
    MsgResponseBase<GetUsersResponse> GetUsers(GetUsersRequest request);
    MsgResponseBase<GetUserResponse> GetUser(GetUserRequest request);



  }
}
