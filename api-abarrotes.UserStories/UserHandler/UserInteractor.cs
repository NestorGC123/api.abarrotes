﻿using api_abarrotes.User.Entities;
using api_abarrotes.User.Db.UnitOfWork;
using api_abarrotes.Utils.Cofigurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using api_abarrotes.Utils.Enums;
using api_abarrotes.Utils.GenericClass;
using api_abarrotes.InteractorMessages;
using api_abarrotes.Utils.Helpers;
using System.Net.Http;

namespace api_abarrotes.UserStories.UserHandler
{
  #region Alias

  using UserEntity = User.Entities.User;

  #endregion

  public class UserInteractor : IUserInteractor
  {

    public static UserInteractor Create(InteractorConfig config) => new UserInteractor(config);

    private UoWUser uoWUser { get; set; }
    //private UoWCatalogs uoWCatalogs { get; set; }
    public UserInteractor(InteractorConfig config)
    {
      uoWUser = UoWUser.Create(
          connection: config.UserDbConnection);
      //uoWCatalogs = UoWCatalogs.Create(connection: config.DataDbConnection);
    }

    public MsgResponseBase<GetUsersResponse> GetUsers(GetUsersRequest request)
    {
      try
      {
        Expression<Func<UserEntity, bool>> filter = u => u.Status != (int)EntityStatusKey.CODES.DELETE;


        var total = this.uoWUser.UserManager.Users
            .Where(filter)
            .Count();

        List<UserEntity> entities = this.uoWUser.UserManager.Users.ToList();
        List<UserItem> userItems = MapperHelper.Map<List<UserItem>>(entities);

        return MsgResponseBase<GetUsersResponse>.Create(new GetUsersResponse
        {
          Total = total,
          Items = userItems
        }, 200);

      }
      catch (Exception e)
      {
        return MsgResponseBase<GetUsersResponse>.Create(new List<string> { e.Message }, 500);
      }
    }
    public MsgResponseBase<GetUserResponse> GetUser(GetUserRequest request)
    {
      try
      {
        Expression<Func<UserEntity, bool>> filter = u => u.Status != (int)EntityStatusKey.CODES.DELETE;
        Expression<Func<UserEntity, bool>> id_filter = u => u.Id == request.UserId;

        filter = filter.And(id_filter);


        UserEntity entity = this.uoWUser.UserManager.Users.Where(filter).FirstOrDefault();

        if(entity is null)
          return MsgResponseBase<GetUserResponse>.Create(new List<string> { "User was not found" }, 400);

        UserItem userItem = MapperHelper.Map<UserItem>(entity);

        return MsgResponseBase<GetUserResponse>.Create(new GetUserResponse
        {
          Item = userItem
        }, 200);

      }
      catch (Exception e)
      {
        return MsgResponseBase<GetUserResponse>.Create(new List<string> { e.Message }, 500);
      }
    }
    public void Dispose()
    {
      //this.uoWCatalogs.Dispose();
      this.uoWUser.Dispose();

      this.uoWUser = null;
      //this.uoWCatalogs = null;
    }

    

    //public MsgResponseBase<GetUsersResponse> GetUsers(GetUsersRequest request)
    //{
    //try
    //{
    //  int Start = request.Start ?? 0;
    //  int Size = request.Size ?? 10;

    //  // Filtro para los usuarios a obtener
    //  Expression<Func<UserEntity, bool>> filter = null;

    //  if (uoWUser.UserManager.IsInRole(userId: request.UserId, role: RoleKey.SYSADMIN))
    //  {
    //    // retorna todos los usuario
    //    // no se aplica ningun filtro por ahora
    //  }

    //  if (filter is null) throw new Exception("No cuenta con los permisos para obtener esta información");

    //  Expression<Func<UserEntity, bool>> filter_status = o => o.Status != (int)EntityStatusKey.CODES.DELETE;
    //  filter = filter.And(filter_status);

    //  var total = this.uoWUser.UserManager.Users
    //      .Where(filter)
    //      .Count();

    //  IQueryable<UserEntity> query = uoWUser.UserManager.Users
    //      .Where(filter)
    //      .OrderBy(x => x.FirstName);

    //  if (request.Filter)
    //  {
    //    query = query.Skip(Start)
    //        .Take(Size);
    //  }

    //  List<UserEntity> entities = query.ToList();

    //  List<ListUserItem> users = new List<ListUserItem>();
    //  foreach (var entity in entities)
    //  {

    //    string rolename = (from userRole in entity.Roles
    //                       join role in uoWUser.RoleManager.Roles on userRole.RoleId equals role.Id
    //                       select role.Name).FirstOrDefault();

    //    AuthorizedThirdParty third = (rolename == RoleKey.AUTH || RoleKey.AUTH_ESP == rolename) ?
    //        this.uoWCatalogs.AuthorizedThirdPartyRepository.Get(o => o.UserId == entity.Id).FirstOrDefault() :
    //        null;

    //    users.Add(new ListUserItem
    //    {
    //      Id = entity.Id,
    //      FirstName = entity.FirstName,
    //      LastName = entity.LastName,
    //      Email = entity.Email,
    //      UserName = entity.UserName,
    //      Telephone = entity.PhoneNumber,
    //      RoleName = string.IsNullOrEmpty(rolename) ?
    //            string.Empty :
    //            RoleKey.TranslateCodeToEspanish(rolename),
    //      AseaRegister = third is null ? null : third.AseaRegister,
    //      EndDate = third is null ? null : (third.EndDate is null ? null : third.EndDate.Value.ToDateSlashFormat()),
    //      InitialDate = third is null ? null : (third.InitialDate is null ? null : third.InitialDate.Value.ToDateSlashFormat()),

    //    });
    //  }

    //  return MsgResponseBase<ListUserResponse>.Create(new ListUserResponse
    //  {
    //    Total = total,
    //    Items = users
    //  });
    //}
    //catch (Exception e)
    //{
    //  return MsgResponseBase<ListUserResponse>.Create(new List<string> { e.Message });
    //}
    //}
  }
}
