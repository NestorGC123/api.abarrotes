﻿using api_abarrotes.InteractorMessages;
using api_abarrotes.User.Db.UnitOfWork;
using api_abarrotes.User.Entities;
using api_abarrotes.Utils.Cofigurations;
using api_abarrotes.Utils.Enums;
using api_abarrotes.Utils.GenericClass;
using api_abarrotes.Utils.Helpers;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.UserStories.RoleHandler
{
  public class RoleInteractor : IRoleInteractor
  {
    public static RoleInteractor Create(InteractorConfig config) => new RoleInteractor(config);

    public MsgResponseBase<GetRolesResponse> GetRoles(GetRolesRequest request)
    {
      try
      {
        Expression<Func<Role, bool>> filter = r => r.Status != (int)EntityStatusKey.CODES.DELETE;


        var total = this.uoWUser.RoleManager.Roles
            .Where(filter)
            .Count();

        List<Role> entities = this.uoWUser.RoleManager.Roles.ToList();
        List<RoleItem> roleItems = MapperHelper.Map<List<RoleItem>>(entities);

        return MsgResponseBase<GetRolesResponse>.Create(new GetRolesResponse
        {
          Total = total,
          Items = roleItems
        }, 200);

      }
      catch(Exception e)
      {
        return MsgResponseBase<GetRolesResponse>.Create(new List<string> { e.Message }, 500);
      }
    }

    public void Dispose()
    {
      this.uoWUser.Dispose();

      this.uoWUser = null;
    }

    public MsgResponseBase<CreateRolesResponse> CreateRoles(CreateRolesRequest request)
    {
      try
      {
        var entities = new List<Role>();
        foreach (var item in request.Items)
        {
          entities.Add(new Role
          {
            Id = Guid.NewGuid().ToString(),
            Name = item.RoleName,
            CreateBy = request.UserId,
            CreateDate = DateTime.Now,
            Status = (int)EntityStatusKey.CODES.NEW
          });
        }

        foreach (var item in entities)
        {
          this.uoWUser.RoleManager.Create(item);
        }

        return MsgResponseBase<CreateRolesResponse>.Create(new CreateRolesResponse
        {
          Items = MapperHelper.Map<List<RoleItem>>(entities),
        }, 201);
      
      }
      catch (Exception e)
      {
        return MsgResponseBase<CreateRolesResponse>.Create(new List<string> { e.Message }, 500);
      }
    }

    private UoWUser uoWUser { get; set; }
    //private UoWCatalogs uoWCatalogs { get; set; }
    public RoleInteractor(InteractorConfig config)
    {
      uoWUser = UoWUser.Create(
          connection: config.UserDbConnection);
      //uoWCatalogs = UoWCatalogs.Create(connection: config.DataDbConnection);
    }
  }
}
