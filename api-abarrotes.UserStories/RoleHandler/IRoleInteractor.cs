﻿using api_abarrotes.Utils.GenericClass;
using api_abarrotes.InteractorMessages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.UserStories.RoleHandler
{
  public interface IRoleInteractor : IDisposable
  {
    MsgResponseBase<GetRolesResponse> GetRoles(GetRolesRequest request);
    MsgResponseBase<CreateRolesResponse> CreateRoles(CreateRolesRequest request);
  }
}
