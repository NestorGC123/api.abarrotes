﻿using api_abarrotes.Abarrotics.Db.UnitOfWork;
using api_abarrotes.Data.Db.UnitOfWork;
using api_abarrotes.InteractorMessages;
using api_abarrotes.Utils.Cofigurations;
using api_abarrotes.Utils.GenericClass;
using Bogus;
using Bogus.DataSets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.UserStories.SaleForecastHandler
{
   public class SaleForecastInteractor : ISaleForecastInteractor
   {
      private UoWAlgoriteamGeneral uoWAlgoriteamGeneral { get; set; }
      private UoWAbarroticsGeneral uoWAbarroticsGeneral { get; set; }
      public static SaleForecastInteractor Create() => new SaleForecastInteractor();
      public SaleForecastInteractor()
      {
         uoWAbarroticsGeneral = UoWAbarroticsGeneral.Create(GlobalConfigurations.ABARROTICS_DB_CONNECTION);
         uoWAlgoriteamGeneral = UoWAlgoriteamGeneral.Create(GlobalConfigurations.ALGORITEAM_DB_CONNECTION);
      }
      public MsgResponseBase<GetSaleForecastsResponse> GetSaleForecasts(GetSaleForecastsRequest request)
      {
         //This implementation is just mock data
         try
         {
            //var ventas = uoWAbarroticsGeneral.SaleDbSet.Get()

            Randomizer.Seed = new Random(123456789);
            var saleForecastFaker = new Faker<SaleForecastItem>()
              .RuleFor(x => x.SaleForecast, x => x.Random.Number(1500, 5000));


            //Calculate week of the year for Start and End date
            int s_dayOfTheYear = request.FromDate.DayOfYear;
            int s_year = request.FromDate.Year;
            int e_dayOfTheYear = request.UntilDate.DayOfYear;
            int e_year = request.UntilDate.Year;
            int yearDifference = (e_year - s_year) + 1;


            int year = s_year;
            var saleForecastMockItems = new List<SaleForecastItem>();
            for (int yearNum = 0; yearNum < yearDifference; yearNum++)
            {
               int weeksInTheYear = (int)Math.Ceiling(DateTime.IsLeapYear(year) ? 366 / 7.0 : 365 / 7.0);
               int daysInTheYear = DateTime.IsLeapYear(year) ? 366 : 365;

               //Si es el año inicial
               if (year == s_year && year != e_year)
               {
                  int s_weekOfTheYear = (int)Math.Ceiling(s_dayOfTheYear / 7.0);

                  for (int week = s_weekOfTheYear; week <= weeksInTheYear; week++)
                  {

                     var fake = saleForecastFaker.Generate();
                     fake.ValidForYear = year;
                     fake.ValidForWeekOfTheYear = week;

                     int? month;
                     int? day;

                     //Start day of the year for this week
                     int validFromDayOfTheYear = ((week - 1) * 7) + 1;
                     CalculateMonthAndDay(year, validFromDayOfTheYear, out month, out day);
                     fake.ValidFromDate = new DateTime(year, month.Value, day.Value, 0, 0, 0, DateTimeKind.Utc);

                     //End day of the year for this week
                     int validUntilDayOfTheYear = week * 7;
                     if (validUntilDayOfTheYear > daysInTheYear) validUntilDayOfTheYear = daysInTheYear;
                     CalculateMonthAndDay(year, validUntilDayOfTheYear, out month, out day);
                     fake.ValidUntilDate = new DateTime(year, month.Value, day.Value, 0, 0, 0, DateTimeKind.Utc);

                     saleForecastMockItems.Add(fake);
                  }

               }

               //Si es el año final
               if (year == e_year && year != s_year)
               {
                  int e_weekOfTheYear = (int)Math.Ceiling(e_dayOfTheYear / 7.0);

                  for (int week = 1; week <= e_weekOfTheYear; week++)
                  {

                     var fake = saleForecastFaker.Generate();
                     fake.ValidForYear = year;
                     fake.ValidForWeekOfTheYear = week;

                     int? month;
                     int? day;

                     //Start day of the year for this week
                     int validFromDayOfTheYear = ((week - 1) * 7) + 1;
                     CalculateMonthAndDay(year, validFromDayOfTheYear, out month, out day);
                     fake.ValidFromDate = new DateTime(year, month.Value, day.Value, 0, 0, 0, DateTimeKind.Utc);

                     //End day of the year for this week
                     int validUntilDayOfTheYear = week * 7;
                     if (validUntilDayOfTheYear > daysInTheYear) validUntilDayOfTheYear = daysInTheYear;
                     CalculateMonthAndDay(year, validUntilDayOfTheYear, out month, out day);
                     fake.ValidUntilDate = new DateTime(year, month.Value, day.Value, 0, 0, 0, DateTimeKind.Utc);

                     saleForecastMockItems.Add(fake);
                  }
               }

               //Si es en un mismo año
               if (year == s_year && year == e_year)
               {
                  int s_weekOfTheYear = (int)Math.Ceiling(s_dayOfTheYear / 7.0);
                  int e_weekOfTheYear = (int)Math.Ceiling(e_dayOfTheYear / 7.0);

                  for (int week = s_weekOfTheYear; week <= e_weekOfTheYear; week++)
                  {

                     var fake = saleForecastFaker.Generate();
                     fake.ValidForYear = year;
                     fake.ValidForWeekOfTheYear = week;

                     int? month;
                     int? day;

                     //Start day of the year for this week
                     int validFromDayOfTheYear = ((week - 1) * 7) + 1;
                     CalculateMonthAndDay(year, validFromDayOfTheYear, out month, out day);
                     fake.ValidFromDate = new DateTime(year, month.Value, day.Value, 0, 0, 0, DateTimeKind.Utc);

                     //End day of the year for this week
                     int validUntilDayOfTheYear = week * 7;
                     if (validUntilDayOfTheYear > daysInTheYear) validUntilDayOfTheYear = daysInTheYear;
                     CalculateMonthAndDay(year, validUntilDayOfTheYear, out month, out day);
                     fake.ValidUntilDate = new DateTime(year, month.Value, day.Value, 0, 0, 0, DateTimeKind.Utc);

                     saleForecastMockItems.Add(fake);
                  }
               }

            
               //Si es un año intermedio
               if(year != s_year && year != e_year)
               {
                  for (int week = 1; week <= weeksInTheYear; week++)
                  {

                     var fake = saleForecastFaker.Generate();
                     fake.ValidForYear = year;
                     fake.ValidForWeekOfTheYear = week;

                     int? month;
                     int? day;

                     //Start day of the year for this week
                     int validFromDayOfTheYear = ((week - 1) * 7) + 1;
                     CalculateMonthAndDay(year, validFromDayOfTheYear, out month, out day);
                     fake.ValidFromDate = new DateTime(year, month.Value, day.Value, 0, 0, 0, DateTimeKind.Utc);

                     //End day of the year for this week
                     int validUntilDayOfTheYear = week * 7;
                     if (validUntilDayOfTheYear > daysInTheYear) validUntilDayOfTheYear = daysInTheYear;
                     CalculateMonthAndDay(year, validUntilDayOfTheYear, out month, out day);
                     fake.ValidUntilDate = new DateTime(year, month.Value, day.Value, 0, 0, 0, DateTimeKind.Utc);

                     saleForecastMockItems.Add(fake);
                  }
               }
               
               year++;
            }

            var total = saleForecastMockItems.Count();
            var saleForecastItems = saleForecastMockItems.ToList();

            //Codigo nuevo


            return MsgResponseBase<GetSaleForecastsResponse>.Create(new GetSaleForecastsResponse
            {
               Total = total,
               Items = saleForecastItems
            }, 200);
         }
         catch (Exception e)
         {
            return MsgResponseBase<GetSaleForecastsResponse>.Create(new List<string> { e.Message }, 500);
         }
      }
      private void CalculateMonthAndDay(int year, int dayOfTheYear, out int? month, out int? day)
      {
         month = null;
         day = null;

         int daysSummatory = 0;
         foreach(int i in new int[] {1,2,3,4,5,6,7,8,9,10,11,12})
         {
            daysSummatory += DateTime.DaysInMonth(year, i);
            if (daysSummatory >= dayOfTheYear)
            {
               month = i;
               break;
            }
         }

         day = dayOfTheYear - (daysSummatory - DateTime.DaysInMonth(year, month.Value));
      }
      public void Dispose()
      {
         //throw new NotImplementedException();
      }
   }
}
