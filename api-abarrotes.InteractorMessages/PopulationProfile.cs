﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.InteractorMessages
{
   public class PopulationProfileItem
   {
      public string AgeRange { get; set; }
      public int NumberOfPeople { get; set; }
      public string PercentageOfPeople { get; set; }
   }
   public class GetPopulationProfilesRequest
   {
      public string StoreId { get; set; }
      public string Region { get; set; }
   }
   public class GetPopulationProfilesResponse
   {
      public int Total { get; set; }
      public List<PopulationProfileItem> Items { get; set; }
   }
}
