﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.InteractorMessages
{
  public class BestsellerItem
  {
    public string Name { get; set; }
    public string Price { get; set; }
  }

  public class GetBestsellersRequest: BaseFilterMessage
  {
    public string StoreId { get; set; }
    public string Region { get; set; }
    public string Date { get; set; }
  }
  public class GetBestsellersResponse
  {
    public int Total { get; set; }
    public List<BestsellerItem> Items { get; set; }
  }

}
