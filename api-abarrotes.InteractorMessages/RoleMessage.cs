﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.InteractorMessages
{
  public class RoleItem
  {
    public string Id { get; set; }
    public string Name { get; set; }
  }

  public class GetRolesResponse
  {
    public int Total { get; set; }
    public List<RoleItem> Items { get; set; }
  }
  public class GetRolesRequest
  {
    public int? Start { get; set; }
    public int? Size { get; set; }
    public string UserId { get; set; }
  }
  public class CreateRolesItem
  {
    public string RoleName { get; set; }
  }
  public class CreateRolesRequest
  {
    public string UserId { get; set; }
    public List<CreateRolesItem> Items { get; set; }
  }
  public class CreateRolesResponse
  {
    public List<RoleItem> Items { get; set; }
  }

}
