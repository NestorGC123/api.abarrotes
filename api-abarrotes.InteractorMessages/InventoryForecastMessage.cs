﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.InteractorMessages
{
   public class InventoryForecastItem
   {
      /// <summary>
      /// Product id
      /// </summary>
      public string ProductId { get; set; }
      /// <summary>
      /// Product name
      /// </summary>
      public string ProductName { get; set; }
      /// <summary>
      /// Inventory forecast for this product
      /// </summary>
      public int InventoryForecast { get; set; }
      public DateTime ValidFromDate { get; set; }
      public DateTime ValidUntilDate { get; set; }
   }

   public class GetInventoryForecastsRequest : BaseFilterMessage
   {
      /// <summary>
      /// Date for which to get an inventory forecast, in ISO format and UTC time, defaults to NOW.
      /// </summary>
      public DateTime ReferenceDate { get; set; }
      /// <summary>
      /// Store id for which to get product inventory forecasts
      /// </summary>
      public string StoreId { get; set; }
   }
   public class GetInventoryForecastsResponse
   {
      public int Total { get; set; }
      public List<InventoryForecastItem> Items { get; set; }
   }
}
