﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.InteractorMessages
{
   public class PriceOptimizationItem
   {
      /// <summary>
      /// Product id
      /// </summary>
      public string ProductId { get; set; }
      /// <summary>
      /// Product name
      /// </summary>
      public string ProductName { get; set; }
      /// <summary>
      /// Lowest price for this product on the specified region
      /// </summary>
      public double LowestPrice { get; set; }
      /// <summary>
      /// Average price for this product on the specified region
      /// </summary>
      public double AveragePrice { get; set; }
      /// <summary>
      /// Highest price for this product on the specified region
      /// </summary>
      public double HighestPrice { get; set; }
   }

   public class GetPriceOptimizationsRequest : BaseFilterMessage
   {
      /// <summary>
      /// Store id for logging purposes
      /// </summary>
      public string StoreId { get; set; }
      /// <summary>
      /// Region for which to get price data and filter results
      /// </summary>
      public string Region { get; set; }
      /// <summary>
      /// Product name to filter results
      /// </summary>
      public string ProductName { get; set; }
   }

   public class GetPriceOptimizationsResponse
   {
      public int Total { get; set; }
      public List<PriceOptimizationItem> Items { get; set; }
   }
}
