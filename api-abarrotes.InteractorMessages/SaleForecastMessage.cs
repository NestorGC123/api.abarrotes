﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.InteractorMessages
{
   public class SaleForecastItem
   {
      public int SaleForecast { get; set; }
      public DateTime ValidFromDate { get; set; }
      public DateTime ValidUntilDate { get; set; }
      public int ValidForWeekOfTheYear { get; set; }
      public int ValidForYear { get; set; }
   }

   public class GetSaleForecastsRequest
   {
      /// <summary>
      /// Store id for which to get sale forecasts
      /// </summary>
      public string StoreId { get; set; }
      /// <summary>
      /// Specify how the forecasts should be aggregated, at this time the only valid string is: Semana
      /// </summary>
      public string AggregationType { get; set; }
      /// <summary>
      /// Date to start sale forecasts search, in ISO format and UTC time
      /// </summary>
      public DateTime FromDate { get; set; }
      /// <summary>
      /// Date to end sale forecasts search, in ISO format and UTC time
      /// </summary>
      public DateTime UntilDate { get; set; }
   }
   public class GetSaleForecastsResponse
   {
      public int Total { get; set; }
      public List<SaleForecastItem> Items { get; set; }
   }
}
