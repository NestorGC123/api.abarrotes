﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.InteractorMessages
{
   public class PromotionItem
   {
      /// <summary>
      /// Promotion's id
      /// </summary>
      public string PromotionId { get; set; }
      /// <summary>
      /// Promotion's description, made of product names
      /// </summary>
      public string Description { get; set; }
      /// <summary>
      /// Summatory of product prices in this promotion, taking their quantity into account
      /// </summary>
      public string PromotionCost { get; set; }
      /// <summary>
      /// Products in this promotion
      /// </summary>
      public List<PromotionProductItem> Products { get; set; }
   }
   public class PromotionProductItem
   {
      /// <summary>
      /// Product's id
      /// </summary>
      public string ProductId { get; set; }
      /// <summary>
      /// Product's name
      /// </summary>
      public string ProductName { get; set; }
      /// <summary>
      /// Product's quantity in this promotion
      /// </summary>
      public int ProductQuantity { get; set; } = 1;
   }
   
   public class GetPromotionsRequest
   {
      public string StoreId { get; set; }
   }
   public class GetPromotionsResponse
   {
      public int Total { get; set;  }
      public List<PromotionItem> Items { get; set; }
   }
}
