﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.InteractorMessages
{
  public class UserItem
  {
    public string Id { get; set; }
    public string UserName { get; set; }
  }
  public class GetUsersRequest
  {
    public int? Start { get; set; }
    public int? Size { get; set; }
    public string UserId { get; set; }

  }
  public class GetUsersResponse
  {
    public int Total { get; set; }
    public List<UserItem> Items { get; set; }

  }
  public class GetUserRequest
  {
    /// <summary>
    /// Id of the User to get
    /// </summary>
    public string UserId { get; set; }

  }
  public class GetUserResponse
  {
    public UserItem Item { get; set; }

  }

}
