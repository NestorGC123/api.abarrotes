﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.InteractorMessages
{
  public class BaseFilterMessage
  {
    #region pagination
    public int Start { get; set; } 
    public int Size { get; set; }
    #endregion
  }
}
