﻿using System;
using System.Linq;

namespace api_abarrotes.Utils.OdbcProvider
{
   public class OdbcDbContext : IDisposable
   {
      private bool Disposed = false;
      private string ConnectionStringName = null;
      public OdbcQueryExecutor Database { get; private set; }
      public OdbcDbContext(string connectionStringName)
      {
         ConnectionStringName = connectionStringName;
         Database = OdbcQueryExecutor.Create(ConnectionStringName);
         initializeOdbcDbSets();
      }
      protected virtual void initializeOdbcDbSets() { }
      public virtual OdbcDbSet<TEntity> GetOdbcDbSet<TEntity>() where TEntity : class, new()
      {
         var setProperty = this.GetType().GetProperties().Where(p => p.PropertyType.Equals(typeof(OdbcDbSet<TEntity>))).FirstOrDefault();
         
         if(setProperty != null)
         {
            return (OdbcDbSet<TEntity>) setProperty.GetValue(this);
         }

         return null;
      }
      public OdbcQueryExecutor GetDatabase()
      {
         return OdbcQueryExecutor.Create(ConnectionStringName);
      }

      public virtual void Dispose(bool disposing)
      {
         if (!Disposed)
         {
            Database.Dispose();
            Database = null;
         }
         Disposed = true;
      }
      public void Dispose()
      {
         Dispose(true);
         GC.SuppressFinalize(true);
      }
   }
}
