﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.Utils.OdbcProvider
{
   public class OdbcWhereGroup
   {
      [Required]
      public List<object> Wheres { get; private set; }
      public bool AppendAsAnd { get; private set; }
      public OdbcWhereGroup(bool appendAsAnd = true)
      {
         Wheres = new List<object>();
         AppendAsAnd = appendAsAnd;
      }
      public void AppendOdbcWhere(OdbcWhere where)
      {
         Wheres.Add(where);
      }
      public void AppendOdbcWhereGroup(OdbcWhereGroup whereGroup)
      {
         Wheres.Add(whereGroup);
      }
      public string GetWhereGroupSqlString(List<OdbcPropertyFieldMap> propertyFieldMaps)
      {
         string sqlString = "";
         int wheresQuantity = Wheres.Count();

         for(var i = 0; i < wheresQuantity; i++)
         {
            var where = Wheres[i];

            if (where.GetType() == typeof(OdbcWhere))
            {
               var odbcWhere = where as OdbcWhere;
               string sql = string.Format("{0} = {1} ", propertyFieldMaps.Where(pfm => pfm.PropertyName == odbcWhere.PropertyName).Single().FieldName, odbcWhere.PropertyValue);
               if (i != wheresQuantity - 1)
                  sql += odbcWhere.AppendAsAnd ? "AND " : "OR ";

               sqlString += sql;
            }

            if (where.GetType() == typeof(OdbcWhereGroup))
            {
               var odbcWhereGroup = where as OdbcWhereGroup;
               string sql = odbcWhereGroup.GetWhereGroupSqlString(propertyFieldMaps);
               if (i != wheresQuantity - 1)
                  sql += odbcWhereGroup.AppendAsAnd ? "AND " : "OR ";

               sqlString += sql;
            }
         }

         return sqlString;
      }
   }
   public class OdbcWhere
   {
      [Required]
      public string PropertyName { get; set; }
      [Required]
      public object PropertyValue { get; set; }
      public bool AppendAsAnd { get; set; } = true;
      public OdbcWhere(string propertyName, object propertyValue, bool appendAsAnd = true)
      {
         PropertyName = propertyName;
         PropertyValue = propertyValue;
         AppendAsAnd = appendAsAnd;
      }
   }
   public class OdbcPropertyFieldMap
   {
      public string PropertyName { get; set; }
      public string FieldName { get; set; }
   }
   public class OdbcDbSet<T> where T : new()
   {
      private OdbcDbContext Context;
      public string TableName { get; private set; }
      public List<OdbcPropertyFieldMap> PropertyFieldMaps { get; private set; }

      public OdbcDbSet(OdbcDbContext database, string tableName, List<OdbcPropertyFieldMap> propertyFieldMaps)
      {
         Context = database;
         TableName = tableName;
         PropertyFieldMaps = propertyFieldMaps;
      }

      public List<T> GetAll()
      {
         Dictionary<string, object> parameters = new Dictionary<string, object>();
         parameters.Add("@ColumnNames", string.Join(",", PropertyFieldMaps.Select(p => p.FieldName).ToList()));
         parameters.Add("@TableName", TableName);

         List<Dictionary<string, string>> rows = Context.Database.Query("SELECT @ColumnNames FROM @TableName;", parameters);

         List<T> items = OdbcResultsToEntities(rows);
         return items;
      }
      //public List<T> Get(int offset = 0, int limit = 10, OdbcWhereGroup wheres = null)
      //{
      //   Dictionary<string, object> parameters = new Dictionary<string, object>();
      //   string query = "SELECT @ColumnNames FROM @TableName";
      //   parameters.Add("@ColumnNames", string.Join(",", PropertyFieldMaps.Select(p => p.FieldName).ToList()));
      //   parameters.Add("@TableName", TableName);

      //   if (wheres != null)
      //   {
      //      query += " WHERE @Where";
      //      parameters.Add("@Where", wheres.GetWhereGroupSqlString(PropertyFieldMaps));
      //   }

      //   query += " LIMIT @Offset,@Limit;";
      //   parameters.Add("@Offset", offset);
      //   parameters.Add("@Limit", limit);

      //   List<Dictionary<string, string>> rows = Context.Database.Query(query, parameters);

      //   List<T> items = new List<T>();
      //   foreach (Dictionary<string, string> row in rows)
      //   {
      //      T item = new T();
      //      foreach (var cell in row)
      //      {
      //         string propertyName = PropertyFieldMaps.Where(pfm => pfm.FieldName == cell.Key.Replace(TableName + ".", "")).FirstOrDefault().PropertyName;
      //         var property = typeof(T).GetProperty(propertyName);
      //         var propertyType = property.PropertyType;

      //         if (cell.Value is null)
      //            continue;

      //         if (propertyType.Equals(typeof(int)))
      //            property.SetValue(item, int.Parse(cell.Value));
      //         if (propertyType.Equals(typeof(bool)))
      //            property.SetValue(item, bool.Parse(cell.Value));
      //         if (propertyType.Equals(typeof(double)))
      //            property.SetValue(item, double.Parse(cell.Value));
      //         if (propertyType.Equals(typeof(float)))
      //            property.SetValue(item, float.Parse(cell.Value));
      //         if (propertyType.Equals(typeof(string)))
      //            property.SetValue(item, cell.Value);
      //      }
      //      items.Add(item);
      //   }
      //   return items;
      //}
      /// <summary>
      /// 
      /// </summary>
      /// <param name="offset">How many element should be skipped, defaults to 0</param>
      /// <param name="limit">How many results to retrieve, defaults to 10</param>
      /// <param name="where">HiveQL string for where clause</param>
      /// <returns></returns>
      public List<T> Get(int offset = 0, int limit = 10, string where = null)
      {
         Dictionary<string, object> parameters = new Dictionary<string, object>();
         string query = "SELECT @ColumnNames FROM @TableName";
         parameters.Add("@ColumnNames", string.Join(",", PropertyFieldMaps.Select(p => p.FieldName).ToList()));
         parameters.Add("@TableName", TableName);

         if (where != null)
         {
            query += " WHERE @Where";
            where = ReplacePropertyForField(where);
            parameters.Add("@Where", where);
         }

         query += " LIMIT @Offset,@Limit;";
         parameters.Add("@Offset", offset);
         parameters.Add("@Limit", limit);

         List<Dictionary<string, string>> rows = Context.Database.Query(query, parameters);

         var items = OdbcResultsToEntities(rows);
         return items;
      }
      public int Count()
      {
         Dictionary<string, object> parameters = new Dictionary<string, object>();
         string query = "SELECT COUNT(*) FROM @TableName";
         parameters.Add("@TableName", TableName);

         //This query is supposed to return just one row
         List<Dictionary<string, string>> rows = Context.Database.Query(query, parameters);

         var row = rows[0];
         var cell = row.First();
         var result = cell.Value;

         return int.Parse(result);
      }

      public string GetFieldName(string propertyName)
      {
         var propertyFieldMap = PropertyFieldMaps.First(pfm => pfm.PropertyName == propertyName);
         return propertyFieldMap != null ? propertyFieldMap.FieldName : null;
      }
      private string ReplacePropertyForField(string command)
      {
         foreach(var pfm in PropertyFieldMaps)
         {
            command = command.Replace(pfm.PropertyName, pfm.FieldName);
         }

         return command;
      }
      public List<T> OdbcResultsToEntities(List<Dictionary<string, string>> odbcResult)
      {
         List<T> items = new List<T>();
         foreach (Dictionary<string, string> row in odbcResult)
         {
            T item = new T();
            foreach (var cell in row)
            {
               string propertyName = PropertyFieldMaps.Where(pfm => pfm.FieldName == cell.Key.Replace(TableName + ".", "")).FirstOrDefault().PropertyName;
               var property = typeof(T).GetProperty(propertyName);
               var propertyType = property.PropertyType;

               if (cell.Value is null)
                  continue;

               if (propertyType.Equals(typeof(int)))
                  property.SetValue(item, int.Parse(cell.Value));
               if (propertyType.Equals(typeof(bool)))
                  property.SetValue(item, bool.Parse(cell.Value));
               if (propertyType.Equals(typeof(double)))
                  property.SetValue(item, double.Parse(cell.Value));
               if (propertyType.Equals(typeof(float)))
                  property.SetValue(item, float.Parse(cell.Value));
               if (propertyType.Equals(typeof(string)))
                  property.SetValue(item, cell.Value);
            }
            items.Add(item);
         }
         return items;
      }
   }
}
