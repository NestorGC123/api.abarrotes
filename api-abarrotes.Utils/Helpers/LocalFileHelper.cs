﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace api_abarrotes.Utils.Helpers
{
  /// <summary>
  /// Clase que se encarga del manejo de  los archivos que serán guardados en el server
  /// </summary>
  public class LocalFileHelper
  {

    #region singleton
    /// <summary>
    /// Crea una instancia de la clase
    /// </summary>
    /// <returns></returns>
    ///

    private static LocalFileHelper Instance { get; set; }

    public static LocalFileHelper GetInstance()
    {
      if (Instance == null)
      {
        Instance = new LocalFileHelper();
      }
      return Instance;
    }

    private LocalFileHelper() { }

    #endregion

    public LocalFileHelperResults SaveBytes(String BasePath, String Filename, byte[] Bytes)
    {
      FileStream _Writer = null;
      try
      {
        bool _DirectoryExist = CreateFolderIfNeeded(BasePath);
        if (_DirectoryExist)
        {
          var FullPath = Path.Combine(BasePath, Filename);
          _Writer = new FileStream(FullPath, FileMode.Create, FileAccess.Write);
          _Writer.Write(Bytes, 0, Bytes.Length);
          return LocalFileHelperResults.Success();
        }
        return LocalFileHelperResults.Fail();
      }
      catch (Exception e)
      {
        return string.IsNullOrEmpty(e.Message) ? LocalFileHelperResults.Fail(e.ToString()) : LocalFileHelperResults.Fail(e.Message);
      }
      finally
      {
        if (_Writer != null) _Writer.Close();
      }
    }

    /// <summary>
    /// Elimina un archivo del server usando el httpcontext.
    /// </summary>
    /// <param name="FilePath">Ruta donde se encuentra el archivo a eliminar.</param>
    /// <returns></returns>
    public LocalFileHelperResults RemoveFile(String PathToRemove)
    {
      try
      {
        if (File.Exists(PathToRemove))
        {
          File.Delete(PathToRemove);
          return LocalFileHelperResults.Create(Completed: true, Msg: "Archivo eliminado");
        }
        return LocalFileHelperResults.Create(Completed: true, Msg: "No existe archivo a eliminar");
      }
      catch (Exception e)
      {
        return LocalFileHelperResults.Create(Completed: false, Msg: "Ha ocurrido un error al eliminar archivo: " + e.Message);
      }
    }

    public bool ExistFile(string Path)
    {
      return File.Exists(Path);
    }

    public byte[] ReadByteFile(string path)
    {
      var stream = File.ReadAllBytes(path);
      return stream;
    }

    /// <summary>
    /// Crea un directorio si este no existe.
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public bool CreateFolderIfNeeded(string path)
    {
      bool result = true;
      if (!Directory.Exists(path))
      {
        Directory.CreateDirectory(path);
      }
      return result;
    }


    public LocalFileHelperResults ReadFile(string Path)
    {
      TextReader _Reader = null;
      var Content = String.Empty;
      try
      {
        _Reader = new StreamReader(Path, encoding: System.Text.Encoding.UTF8);
        Content = _Reader.ReadToEnd();
        return LocalFileHelperResults.Create(true, "Completado", Content);
      }
      catch (Exception e)
      {
        return LocalFileHelperResults.Create(false, e.Message);
      }
      finally
      {
        if (_Reader != null) _Reader.Close();
      }
    }

    public LocalFileHelperResults DeleteFile(string Path)
    {
      try
      {
        if (File.Exists(Path))
        {
          File.Delete(Path);

        }
        return LocalFileHelperResults.Create(true, "Completado");
      }
      catch (Exception e)
      {
        return LocalFileHelperResults.Create(false, e.Message);
      }

    }

    public LocalFileHelperResults DeleteDirectory(string Path)
    {
      try
      {
        List<string> DirectoryFiles = Directory.GetFiles(Path, "*", SearchOption.AllDirectories).ToList();
        foreach (var file in DirectoryFiles)
        {
          File.Delete(file);
        }
        return LocalFileHelperResults.Create(true, "Completado");
      }
      catch (Exception e)
      {
        return LocalFileHelperResults.Create(false, e.Message);
      }

    }

    #region Class LocalFileHelper Results

    /// <summary>
    /// Clase para manejo de resultaods del File Handler
    /// </summary>
    public class LocalFileHelperResults
    {

      public static LocalFileHelperResults Success(String Msg = "Completado") => new LocalFileHelperResults()
      {
        Completed = true,
        Msg = Msg
      };

      public static LocalFileHelperResults Fail(String Msg = "ha ocurrido un error inesperado") => new LocalFileHelperResults()
      {
        Completed = false,
        Msg = Msg
      };

      /// <summary>
      /// Genera una instancia de la clase
      /// </summary>
      /// <param name="Completed"> valor boleano que indica si se completo el proceso</param>
      /// <param name="Msg">Algun mensaje del resultado del proceso </param>
      /// <returns></returns>
      public static LocalFileHelperResults Create(bool Completed, String Msg, String Data = "")
      {
        return new LocalFileHelperResults()
        {
          Msg = Msg,
          Data = Data,
          Completed = Completed
        };
      }

      /// <summary>
      /// Mensaje de algun estado
      /// </summary>
      public String Msg { get; set; }

      /// <summary>
      /// Bandera que indica si fue correcto el proceso
      /// </summary>
      public bool Completed { get; set; }

      /// <summary>
      /// 
      /// </summary>
      public string Data { get; set; }
    }
    #endregion

  }

}