﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace api_abarrotes.Utils.Helpers
{
   public static class ModelHelper
   {
      public static string GetColumName<T>(string propertyName)
      {
         var property = typeof(T).GetProperty(propertyName);
         ColumnAttribute columnAttr = (ColumnAttribute)property.GetCustomAttribute(typeof(ColumnAttribute), false);

         return columnAttr != null ? columnAttr.Name : null;
      }
      public static Dictionary<string, string> GetColums<T>()
      {
         Dictionary<string, string> output = new Dictionary<string, string>();
         var properties = typeof(T).GetProperties();

         foreach(PropertyInfo property in properties)
         {
            var columnName = GetColumName<T>(property.Name);
            if(columnName != null)
               output.Add(property.Name, columnName);
         }

         return output;
      }
      public static string GetTableName<T>()
      {
         TableAttribute tableAttr = (TableAttribute) typeof(T).GetCustomAttribute(typeof(TableAttribute), false);

         return tableAttr != null ? tableAttr.Name : null;
      }
      public static List<T> OdbcResultToModel<T>(Dictionary<string, string> odbcResult)
      {
         if (odbcResult == null)
            return null;

         var columns = GetColums<T>();

         return null;
      }
   }
}
