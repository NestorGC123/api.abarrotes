﻿using AutoMapper;
using api_abarrotes.InteractorMessages;
using api_abarrotes.ModelViews;
using api_abarrotes.User.Entities;
using UserEntity = api_abarrotes.User.Entities.User;
using api_abarrotes.Data.Entities;

namespace api_abarrotes.Utils.Helpers
{
   public class MapperHelper
   {
      internal static IMapper mapper;

      static MapperHelper()
      {
         var config = new MapperConfiguration(x =>
         {
            x.CreateMap<UserItem, UserEntity>().ReverseMap();
            x.CreateMap<MVBestsellerItem, BestsellerItem>().ReverseMap();
            x.CreateMap<MVUserItem, UserItem>().ReverseMap();
            x.CreateMap<RoleItem, Role>().ReverseMap();
            x.CreateMap<MVRoleItem, RoleItem>().ReverseMap();
            x.CreateMap<CreateRolesItem, MVCreateRolesItem>().ReverseMap();
            x.CreateMap<MVInventoryForecastItem, InventoryForecastItem>().ReverseMap();
            x.CreateMap<MVSaleForecastItem, SaleForecastItem>().ReverseMap();
            x.CreateMap<MVPriceOptimizationItem, PriceOptimizationItem>().ReverseMap();
            x.CreateMap<MVPromotionItem, PromotionItem>().ReverseMap();
            x.CreateMap<MVPromotionProductItem, PromotionProductItem>().ReverseMap();
            x.CreateMap<MVPopulationProfileItem, PopulationProfileItem>().ReverseMap();

            //x.CreateMap<InventoryForecastItem, PrediccionInventario>().ReverseMap();
         });

         mapper = config.CreateMapper();
      }

      public static T Map<T>(object source)
      {
         return mapper.Map<T>(source);
      }
   }
}
