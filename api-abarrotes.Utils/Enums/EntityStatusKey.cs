﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.Utils.Enums
{
  public class EntityStatusKey
  {
    public static readonly string NEW = "New";
    public static readonly string EDIT = "Edited";
    public static readonly string DELETE = "Deleted";
    public static readonly string UNDEFINED = "Undefined";

    public enum CODES
    {
      UNDEFINED = 0,
      NEW = 1,
      EDIT = 2,
      DELETE = 3
    }

    public static string TranslateCode(int code)
    {
      switch (code)
      {
        case (int)CODES.NEW: return NEW;
        case (int)CODES.EDIT: return EDIT;
        case (int)CODES.DELETE: return DELETE;
        default: return UNDEFINED;
      }
    }
  }
}
