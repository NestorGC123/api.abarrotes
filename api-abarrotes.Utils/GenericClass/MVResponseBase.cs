﻿using api_abarrotes.InteractorMessages;
using Swashbuckle.Examples;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.Utils.GenericClass
{
  public class MVResponseBase<T>
  {

    public static MVResponseBase<T> Create(T Data) => new MVResponseBase<T>
    {
      Status = true,
      Data = Data
    };

    public static MVResponseBase<T> Create(List<string> Errors) => new MVResponseBase<T>
    {
      Status = false,
      Errors = Errors
    };
    public bool Status { get; set; }
    public T Data { get; set; }
    public List<string> Errors { get; set; }
  }

/*  public class SwaggerSuccessMVResponseBaseExample<T>
  {
    public bool Status { get; set; } = true;
    public T Data { get; set; }
    public List<string> Errors { get; set; } = null;
  }*/

  public class SwaggerFailureMVResponseBaseExample<T>
  {
    public bool Status { get; set; } = false;
    public T Data { get; set; } = default(T);
    public List<string> Errors { get; set; }
  }
  public class SwaggerSuccessMVResponseBaseExample<T> : IExamplesProvider
  {
    public object GetExamples()
    {
      return new MVResponseBase<T>
      {
        Status = true,
        Data = default(T),
        Errors = null
        };
    }
  }

}
