﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace api_abarrotes.Utils.GenericClass
{
  public class MsgResponseBase<T>
  {
    public MsgResponseBase()
    {
      this.Errors = new List<string>();
    }

    public static MsgResponseBase<T> Create(T Data, int StatusCode) => new MsgResponseBase<T>
    {
      Status = true,
      Data = Data,
      StatusCode = StatusCode,
    };

    public static MsgResponseBase<T> Create(List<string> Errors, int StatusCode) => new MsgResponseBase<T>
    {
      Status = false,
      Errors = Errors,
      StatusCode = StatusCode
    };

    public int StatusCode { get; set; }
    public bool Status { get; set; }
    public T Data { get; set; }
    public List<string> Errors { get; set; }
  }
}
