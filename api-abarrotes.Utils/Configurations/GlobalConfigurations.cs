﻿using System.Configuration;

namespace api_abarrotes.Utils.Cofigurations
{
   public class GlobalConfigurations
   {
      public static string ALGORITEAM_DB_CONNECTION = "AlgoriteamDbContext";
      public static string USER_DB_CONNECTION = "UserDbContext";
      public static string ABARROTICS_DB_CONNECTION = "AbarroticsDbContext";

   }
}
